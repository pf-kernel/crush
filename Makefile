PRJ = crush

all: check

check:
	-mypy -p $(PRJ)
	-pylint $(PRJ)
	-flake8 $(PRJ)

format:
	-isort $(PRJ)
	-black $(PRJ)

.PHONY: check
