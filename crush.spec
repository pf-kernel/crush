Name: {{{ git_dir_name }}}
Version: {{{ git_dir_version }}}
Release: 1%{?dist}
Summary: A drgn-based crash-util replacement
BuildArch: noarch
URL: https://codeberg.org/pf-kernel/crush
VCS: {{{ git_dir_vcs }}}
License: GPLv3+
BuildRequires: python3-devel
Source: {{{ git_dir_pack }}}

%description
crush is an experimental attempt to, at least partially, rewrite crash-util using Python and a more modern drgn backend.

%generate_buildrequires
%pyproject_buildrequires

%prep
{{{ git_dir_setup_macro }}}

%build
%pyproject_wheel

%install
%pyproject_install
%pyproject_save_files %{name}

%files -f %{pyproject_files}
%doc README.md
%license LICENSE
%{_bindir}/crush

%changelog
{{{ git_dir_changelog }}}
