# crush

`crush` is an experimental attempt to, at least partially, rewrite [crash-util](https://github.com/crash-utility/crash/) using Python and a more modern [drgn](https://github.com/crash-utility/crash/) backend.

The code is in its early days, is subjected to a massive rewrite at any point, and important functions are not (_yet_) implemented.

## Usage

Type `python -m crush.crush -h` first.

A vmcore, a vmlinux file and, optionally, a folder with module debug symbols have to be specified. For instance:

```
$ python -m crush.crush -c vmcore -l vmlinux -m folder/with/debuginfo
```

Once started, the tool will greet you with a `crush>` prompt. To get an overview of all currently supported commands, type `help`. For each command, type `<command> -h` to get a more detailed usage info.

## Dependencies

* `drgn`
* `python-asteval`
* `python-capstone`
* `python-prettytable`
* `python-pyxdg`

As a rule of thumb, the latest `drgn` should be used even if it means using a git snapshot.

## Installation

`python-flit` is used as a build backend.

To build a wheel, install `python-build` and run:

```
$ python -m build
```

To install a wheel, make sure `python-installer` is available, and then run:

```
$ python -m installer --destdir=/some/dir dist/crush-…-.whl
```

This will also generate a startup Python script under `<destdir>/usr/bin` named `crush` which should be used instead of invoking `python -m crush.crush`.

Pre-built packages are available:

* for Arch Linux: [here](https://build.opensuse.org/project/show/home:post-factum:crush) [![OBS Status](https://build.opensuse.org/projects/home:post-factum:crush/packages/crush/badge.svg?type=default)](https://build.opensuse.org/package/show/home:post-factum:crush/crush)
* for Fedora: [here](https://copr.fedorainfracloud.org/coprs/pfactum/crush) ![Copr Status](https://copr.fedorainfracloud.org/coprs/pfactum/crush/package/crush/status_image/last_build.png)

The packages are automatically rebuilt on each push.

## Contributing

Feel free to [open an issue](https://codeberg.org/pf-kernel/crush/issues/new), create a [pull request](https://codeberg.org/pf-kernel/crush/pulls), or mail me directly: [oleksandr@redhat.com](mailto:oleksandr@redhat.com).

Please use the following linters on your code:

* `flake8`
* `mypy`
* `pylint`

and format it with:

* `black`
* `isort`

Having the aforementined tools installed, you may just type `make`, and all the checks and formatting adjustments will be invoked automatically.

## Licensing

This tool is being developed by me, a Red Hat engineer, during my business hours, whatever that means for licensing, author- and ownership. The code is licensed under terms and conditions of GPLv3 or, at your option, any later version. See [LICENSE](https://codeberg.org/pf-kernel/crush/src/branch/master/LICENSE) file for more details.
