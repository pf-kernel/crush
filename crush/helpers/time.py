#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from crush.commands.common import Context
from crush.helpers.common import contains


def get_timekeeper(ctx: Context):
    if ctx.symbol_exists("tk_core"):
        timekeeper = ctx.prog["tk_core"].timekeeper
    elif ctx.symbol_exists("timekeeper"):
        timekeeper = ctx.prog["timekeeper"]
    else:
        raise NotImplementedError("Timekeeping structure is not supported")

    return timekeeper


def get_uptime_ns(ctx: Context):
    timekeeper = get_timekeeper(ctx)
    if not contains(timekeeper, "tkr_mono"):
        jiffies = ctx.prog["jiffies"].value_()
        wrapped = jiffies & 0xFFFFFFFF00000000
        if wrapped:
            wrapped -= 0x100000000
            jiffies &= 0x00000000FFFFFFFF
            jiffies |= wrapped
        jiffies += 300 * ctx.config_hz
        uptime_ns = jiffies * (1_000 // ctx.config_hz) * 1_000_000
    elif not contains(timekeeper.tkr_mono.base, "tv64"):
        uptime_ns = (timekeeper.tkr_mono.base + timekeeper.offs_boot).value_()
    else:
        uptime_ns = (timekeeper.tkr_mono.base.tv64 + timekeeper.offs_boot.tv64).value_()
    return uptime_ns
