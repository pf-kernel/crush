#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import List, Optional, Union

from drgn import FaultError, Object
from drgn.helpers.linux.cpumask import for_each_possible_cpu
from drgn.helpers.linux.module import address_to_module
from drgn.helpers.linux.percpu import per_cpu, per_cpu_ptr
from drgn.helpers.linux.slab import find_containing_slab_cache, slab_object_info

from crush.context import Context


class Address:
    """
    Represents a virtual address, possibly with per-CPU indications.

    Attributes
    ----------
    cpu: Optional[int]
        per-CPU index
    percpu: Optional[int]
        per-CPU offset
    addr: int
        full virtual address

    Methods
    -------
    is_percpu():
        checks if the address has per-CPU indicators
    get_cpu():
        returns a per-CPU index
    get_addr():
        returns full virtual address
    """

    def __init__(self, cpu: Optional[int], percpu: Optional[int], addr: int):
        self.cpu = cpu
        self.percpu = percpu
        self.addr = addr

    def is_percpu(self) -> bool:
        return self.percpu is not None

    def get_cpu(self) -> Optional[int]:
        return self.cpu

    def get_addr(self) -> int:
        return self.addr


def bin2ascii(raw: bytes) -> str:
    # TODO: raw.decode("ascii", errors = "replace")
    # but I'd like to have "." (dot) as a replacement marker

    val = []

    for i in raw:
        # although space (32) is printable
        # skip it to make it explicitly visible
        if 33 <= i <= 126:
            val.append(chr(i))
        else:
            val.append(".")

    return "".join(val)


def parse_cpu_range(string: str) -> List[int]:
    val = []

    if string == "a":
        for cpu in for_each_possible_cpu():
            val.append(cpu)
    else:
        for chunk in string.split(","):
            if "-" in chunk:
                left, right = chunk.split("-")
                left_x, left_y = int(left), int(right)
                val.extend(range(left_x, left_y + 1))
            else:
                left_x = int(chunk)
                val.append(left_x)
        for cpu in val:
            if cpu not in for_each_possible_cpu():
                raise ValueError(f"CPU {cpu} is not available")

    return val


def try_int(string: Optional[str], base=16) -> Optional[int]:
    if string is None:
        return None

    try:
        val = int(string, base)
        return val
    except ValueError:
        return None


def resolve(ctx: Context, string: str) -> List[Address]:
    addrs = []

    if ":" in string:
        qual, cpu_str = string.split(":")
        cpu = parse_cpu_range(cpu_str)
    else:
        qual = string
        cpu = None

    if "+" in qual:
        qual, offset_str = qual.split("+")
        offset = int(offset_str, 16)
    else:
        offset = 0

    val: Union[int, None, str] = try_int(qual)
    if val is None:
        val = qual

    if cpu is not None:
        if isinstance(val, int):
            obj = Object(ctx.prog, "void *", val)
            for i in cpu:
                addrs.append(Address(i, val, per_cpu_ptr(obj, i).value_()))
        elif isinstance(val, str):
            obj = ctx.prog[val]
            for i in cpu:
                addrs.append(Address(i, obj.address_, per_cpu(obj, i).address_))
    else:
        if isinstance(val, int):
            addrs.append(Address(None, None, val + offset))
        elif isinstance(val, str):
            addrs.append(Address(None, None, ctx.prog.symbol(val).address + offset))

    return addrs


def demangle(ctx: Context, addr: int) -> Optional[str]:
    # TODO: on s390x NULL is apparently a valid address, but it decodes to
    #       __decode_fail, so just filter it out;
    #       on s390x there's _text symbol, compare the address to it?
    if addr == 0:
        return None

    if ctx.in_symcache(addr):
        return ctx.get_from_symcache(addr)

    try:
        # discard unreadable addresses
        ctx.prog.read_word(addr)
    except FaultError:
        ctx.put_to_symcache(addr, None)
        return None

    try:
        symbol = ctx.prog.symbol(addr)
        offset = addr - symbol.address
        chunk = symbol.name
        if offset != 0:
            chunk += "+" + hex(offset)
        ctx.put_to_symcache(addr, chunk)
        return chunk
    except (LookupError, OverflowError):
        pass

    try:
        kmem_cache = find_containing_slab_cache(addr)
        obj_info = slab_object_info(addr)
    except Exception:  # pylint: disable=broad-exception-caught
        # ^^ broad exception is fixed by:
        #    https://github.com/osandov/drgn/commit/2b67e0991f551208b761cc4b3e2027039d6f469c

        # TODO: PPC64 address translation is WIP
        kmem_cache = None
        obj_info = None

    if kmem_cache is not None and kmem_cache.value_() != 0:
        allocated = obj_info and obj_info.allocated
        chunk = (
            ("[" if allocated else "")
            + kmem_cache.name.string_().decode()
            + ":"
            + hex(addr)
            + ("]" if allocated else "")
        )
        ctx.put_to_symcache(addr, chunk)
        return chunk

    return None


def wreg(ctx: Context, val: int, word_size: Optional[int] = None, x_prefix: bool = True) -> str:
    ret = ""

    if x_prefix:
        ret += "0x"

    if word_size is None:
        word_size = ctx.get_word_size()

    return f"{val:0{word_size * 2}x}"


def address_to_module_name(ctx: Context, addr: int) -> Optional[str]:
    if ctx.in_modcache(addr):
        return ctx.get_from_modcache(addr)

    module = address_to_module(ctx.prog, addr)
    if module.value_() != 0:
        module_name = module.name.string_().decode()
    else:
        module_name = None

    ctx.put_to_modcache(addr, module_name)
    return module_name


def contains(obj: Object, member: str) -> bool:
    try:
        _ = obj.member_(member)
    except (TypeError, LookupError):
        return False
    return True
