#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import List, Optional

from drgn import FaultError, Object
from drgn.helpers.linux.cpumask import for_each_online_cpu
from drgn.helpers.linux.fs import d_path
from drgn.helpers.linux.list import list_for_each_entry
from drgn.helpers.linux.percpu import per_cpu, percpu_counter_sum
from drgn.helpers.linux.pid import find_task, for_each_task
from drgn.helpers.linux.sched import idle_task

from crush.context import Context
from crush.helpers.common import contains, try_int
from crush.textbuffer import TextBuffer

MM_FILEPAGES = 0
MM_ANONPAGES = 1

PF_KTHREAD = 0x00200000

# include/linux/sched/prio.h
MAX_NICE = 19
MIN_NICE = -20
NICE_WIDTH = MAX_NICE - MIN_NICE + 1
MAX_RT_PRIO = 100
DEFAULT_PRIO = MAX_RT_PRIO + NICE_WIDTH // 2


def is_kthread(task: Object) -> bool:
    return task.flags & PF_KTHREAD


def is_group_leader(task: Object) -> bool:
    return task.group_leader == task


def task_handle_to_task(ctx: Context, task_handle: str) -> Optional[Object]:
    # guess int base
    if task_handle.startswith("0x"):
        task_int = try_int(task_handle)
    else:
        task_int = try_int(task_handle, base=10)
        if task_int is None:
            task_int = try_int(task_handle)

    # filter out non-int input
    if task_int is None:
        raise ValueError(f"Unable to parse task handle: {task_handle}")

    # filter out negative numbers
    if task_int < 0:
        raise ValueError(f"Task handle cannot be negative: {task_handle}")

    # try PID
    if task_int == 0:
        # TODO: current CPU concept?
        return ctx.prog["init_task"].address_of_()
    elif 0 < task_int < ctx.prog["pid_max"]:
        task = find_task(task_int)
        if task.value_() != 0:
            return task
        else:
            # TODO: what if pid_max > struct task_struct* on s390x?
            raise ProcessLookupError(f"Task does not exist: {task_handle}")

    # try task_struct address
    try:
        ctx.prog.read_word(task_int)
        return Object(ctx.prog, "struct task_struct *", task_int)
    except FaultError:
        raise ProcessLookupError(f"Task does not exist: {task_handle}")


def task_cpu(task: Object) -> int:
    if contains(task, "cpu"):
        return task.cpu.value_()
    elif contains(task, "wake_cpu"):
        return task.wake_cpu.value_()
    else:
        raise NotImplementedError("`struct task_struct` layout is not supported")


def task_rtprio(task: Object) -> int:
    return task.rt_priority.value_()


def task_niceval(task: Object) -> int:
    static_prio = task.static_prio.value_()
    return static_prio - DEFAULT_PRIO


def task_header(task: Object) -> str:
    pid = task.pid.value_()
    addr = hex(task.value_())
    cpu = task_cpu(task)
    comm = task.comm.string_().decode()

    task_fs = task.fs

    if task_fs.value_() == 0:
        root = "N/A"
        pwd = "N/A"
    else:
        root = d_path(task_fs.root).decode()
        pwd = d_path(task_fs.pwd).decode()

    return f"PID: {pid}\tTASK: {addr}\tCPU: {cpu}\tCOMMAND: {comm}\tROOT: {root}\tPWD: {pwd}"


def tasks_from_stdin(
    ctx: Context,
    pids: List[str],
    stdin: Optional[TextBuffer],
    default_all: bool = False,
) -> List[Object]:
    tasks = []

    if not pids:
        if default_all:
            for i in for_each_task():
                tasks.append(i)
            for i in for_each_online_cpu():
                tasks.append(idle_task(i))
        else:
            tasks.append(ctx.current)
    else:
        for task in pids:
            if task == "-" and stdin is not None:
                for i in stdin:
                    if not i == "":
                        tasks.append(task_handle_to_task(ctx, i.strip()))
            else:
                tasks.append(task_handle_to_task(ctx, task))

    return tasks


def is_active(ctx: Context, task: Object) -> bool:
    cpu = task_cpu(task)
    return per_cpu(ctx.prog["runqueues"], cpu).curr == task


def task_state_to_char(task: Object) -> str:
    EXIT_NONE = 0x0000
    EXIT_DEAD = 0x0010
    EXIT_ZOMBIE = 0x0020
    TASK_RUNNING = 0x0000
    TASK_INTERRUPTIBLE = 0x0001
    TASK_UNINTERRUPTIBLE = 0x0002
    TASK_PARKED = 0x0040
    TASK_WAKEKILL = 0x0100
    TASK_WAKING = 0x0200
    TASK_NOLOAD = 0x0400

    if contains(task, "state"):
        state = task.state.value_()
    elif contains(task, "__state"):
        state = task.__state.value_()
    else:
        raise NotImplementedError("`struct task_struct` layout is not supported")

    exit_state = task.exit_state.value_()

    # TODO: stopped, traced, dead

    if exit_state & EXIT_DEAD or exit_state & EXIT_ZOMBIE:
        return "Z"
    if state == TASK_RUNNING and exit_state == EXIT_NONE:
        return "R"
    if state & TASK_INTERRUPTIBLE and exit_state == EXIT_NONE:
        return "S"
    if state & TASK_NOLOAD and state & TASK_UNINTERRUPTIBLE and exit_state == EXIT_NONE:
        return "I"
    if state & TASK_UNINTERRUPTIBLE and exit_state == EXIT_NONE:
        return "D"
    if state & TASK_WAKING and exit_state == EXIT_NONE:
        return "W"
    if state & TASK_PARKED and exit_state == EXIT_NONE:
        return "P"
    if state & TASK_WAKEKILL and exit_state == EXIT_NONE:
        return "A"

    return "?"


def task_policy_to_str(task: Object) -> str:
    match task.policy:
        case 0:
            return "OT"
        case 1:
            return "FF"
        case 2:
            return "RR"
        case 3:
            return "BA"
        case 4:
            return "IS"
        case 5:
            return "ID"
        case 6:
            return "DL"
        case _:
            return "??"


def task_rss(task: Object) -> Optional[int]:
    # TODO: memory stats calculation differs across kernel versions
    #       some kernels do not have split RSS accounting

    group_leader = task.group_leader
    mm_struct = group_leader.mm
    if mm_struct.value_() == 0:
        return None

    if contains(mm_struct.rss_stat, "count"):
        ret = (
            mm_struct.rss_stat.count[MM_FILEPAGES].counter.value_()
            + mm_struct.rss_stat.count[MM_ANONPAGES].counter.value_()
        )
        thread_list = group_leader.signal.thread_head.address_of_()
        for thread in list_for_each_entry("struct task_struct", thread_list, "thread_node"):
            ret += (
                thread.rss_stat.count[MM_FILEPAGES].value_()
                + thread.rss_stat.count[MM_ANONPAGES].value_()
            )
    else:
        ret = percpu_counter_sum(mm_struct.rss_stat[MM_FILEPAGES]) + percpu_counter_sum(
            mm_struct.rss_stat[MM_ANONPAGES]
        )

    return ret
