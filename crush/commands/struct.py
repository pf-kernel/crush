#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from typing import Optional

from drgn import Object

from crush.commands.common import Command
from crush.helpers.common import resolve


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="struct", description="Show a content of a given structure")
    parser.add_argument("-a", "--member-address", action="store_true", help="show member address")
    parser.add_argument(
        "-d",
        "--no-dereference",
        action="store_true",
        help="do not dereference leaf pointer",
    )
    parser.add_argument(
        "-s", "--no-struct-type", action="store_true", help="do not show struct type"
    )
    parser.add_argument(
        "-t",
        "--member-types",
        action="store_true",
        help="show struct member types",
    )
    parser.add_argument(
        "-Y",
        "--do-not-symbolify",
        action="store_true",
        help="do not resolve addresses into symbols",
    )
    parser.add_argument("structure", help="structure name")
    parser.add_argument("address", help="structure address")
    try:
        args = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    struct_name = args.structure.split(".")[0]
    struct_members = ".".join(args.structure.split(".")[1:]).split(",")

    addrs = []
    if args.address == "-" and cmd.stdin is not None:
        for i in cmd.stdin:
            if not i == "":
                addrs += resolve(cmd.ctx, i.strip())
    else:
        addrs = resolve(cmd.ctx, args.address)

    for addr in addrs:
        struct_addr = addr.get_addr()
        struct = Object(cmd.ctx.prog, f"struct {struct_name} *", struct_addr)

        struct_prefix = ""
        # prepend per-CPU info
        if addr.is_percpu():
            struct_prefix = f"[{addr.get_cpu()}]:" + " "

        if struct_members == [""]:
            # dump the whole struct if no struct members are specified
            # TODO: is no_dereference actually useful here?
            #       how to make members not to be dereferenced,
            #       but still dereference the outer struct?
            struct_str = struct.format_(
                dereference=not args.no_dereference,
                symbolize=not args.do_not_symbolify,
                type_name=not args.no_struct_type,
                member_type_names=args.member_types,
            )
            cmd.println(f"{struct_prefix}{struct_str}")
        else:
            # dump struct type (if not asked otherwise) and address
            if args.no_struct_type:
                cmd.println(f"{struct_prefix}*0x{struct_addr:x} = " + "{")
            else:
                cmd.println(f"{struct_prefix}*({struct.type_})0x{struct_addr:x} = " + "{")

            # go through supplied struct members
            for member in struct_members:
                leaf = struct
                leaf_path_steps = []
                for step in member.split("."):
                    # dig through nested struct members
                    leaf = leaf.member_(step)
                    leaf_path_steps.append(step)

                leaf_prefix = ""
                # dump struct member address within the struct
                if args.member_address:
                    leaf_prefix = f"[0x{leaf.address_:x}]" + " "

                # dump a path to a nested struct member
                leaf_path = ".".join(leaf_path_steps)

                # dump the struct member
                # note that both type name and member type names depend on args.member_types
                # as these are struct members, not the outer struct itself
                leaf_suffix = leaf.format_(
                    dereference=not args.no_dereference,
                    symbolize=not args.do_not_symbolify,
                    type_name=args.member_types,
                    member_type_names=args.member_types,
                )

                # shape the struct member line
                leaf_str = f"{leaf_prefix}.{leaf_path} = {leaf_suffix}"

                # indent struct members
                for leaf_line in leaf_str.split("\n"):
                    cmd.println(f"\t{leaf_line}")

            # close the struct
            cmd.println("}")

    return 0
