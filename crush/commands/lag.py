#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from typing import Optional

from drgn.helpers.linux.cpumask import for_each_online_cpu
from drgn.helpers.linux.percpu import per_cpu
from prettytable import PrettyTable

from crush.commands.common import Command


def format_timestamp(_field: str, value: str) -> str:
    nsecs = int(value)

    if nsecs == -1:
        return "-"

    return f"{nsecs / 1_000_000_000:.3f}"


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="lag", description="Show CPU lag information")
    parser.add_argument("--all", "-a", action="store_true", help="show all CPUs regardless of lag")
    try:
        args = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    try:
        watchdog_thresh = cmd.ctx.prog["watchdog_thresh"].value_() * 1_000_000_000
    except KeyError:
        # TODO: s390x doesn't have it?
        watchdog_thresh = -1

    most_fresh = -1
    most_fresh_cpu = -1
    for cpu in for_each_online_cpu(cmd.ctx.prog):
        rq_clock = per_cpu(cmd.ctx.prog["runqueues"], cpu).clock.value_()
        if rq_clock > most_fresh:
            most_fresh = rq_clock
            most_fresh_cpu = cpu

    cpus = []
    for cpu in for_each_online_cpu(cmd.ctx.prog):
        rq_clock = per_cpu(cmd.ctx.prog["runqueues"], cpu).clock.value_()
        if cpu == most_fresh_cpu:
            cpus.append([cpu, -1, "most recent"])
        else:
            diff = most_fresh - rq_clock
            if watchdog_thresh != -1:
                lags_too_much = diff / watchdog_thresh > 0.75
                lags_too_little = diff / watchdog_thresh < 0.75
                if args.all or not lags_too_little:
                    cpus.append([cpu, diff, "!!!" if lags_too_much else ""])
            else:
                cpus.append([cpu, diff, ""])

    if len(cpus) > 1:
        table = PrettyTable()
        table.border = False
        table.align = "l"
        table.field_names = ["CPU", "LAG, s", "NOTE"]
        table.custom_format["LAG, s"] = format_timestamp

        for i in cpus:
            table.add_row(i)

        cmd.println(table.get_string(sortby="LAG, s"))
    else:
        cmd.println("No lagging CPUs")

    # TODO: also evaluate (hr)timers lag

    return 0
