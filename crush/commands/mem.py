#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from typing import Optional

from drgn import Object
from drgn.helpers.linux.mm import totalram_pages
from prettytable import PrettyTable

from crush.commands.common import Command


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="mem", description="Show memory information")
    try:
        _ = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    table = PrettyTable()
    table.border = False
    table.field_names = ["", "PAGES", "MiB", "%"]
    table.align = "r"

    nr_free_pages_idx = cmd.ctx.prog["NR_FREE_PAGES"]
    if "NR_KERNEL_STACK_KB" in cmd.ctx.prog:
        nr_kernel_stack_idx = cmd.ctx.prog["NR_KERNEL_STACK_KB"]
        nr_kernel_stack_in_kb = True
    elif "NR_KERNEL_STACK" in cmd.ctx.prog:
        nr_kernel_stack_idx = cmd.ctx.prog["NR_KERNEL_STACK"]
        nr_kernel_stack_in_kb = False
    else:
        raise NotImplementedError("Unable to enumerate kernel stack size")

    if cmd.ctx.symbol_exists("vm_zone_stat"):
        zone_stats = cmd.ctx.prog["vm_zone_stat"]
    elif cmd.ctx.symbol_exists("vm_stat"):
        zone_stats = cmd.ctx.prog["vm_stat"]
    else:
        cmd.errln("Unable to get VM zone stats")
        return -1

    if cmd.ctx.symbol_exists("vm_node_stat"):
        node_stats = cmd.ctx.prog["vm_node_stat"]
    else:
        node_stats = None

    page_size = cmd.ctx.get_page_size()

    totalram = totalram_pages(cmd.ctx.prog) * page_size
    totalram_mib = f"{totalram / 2**20:.2f}"

    nr_free_pages = zone_stats[nr_free_pages_idx].counter.value_()

    nr_free_mib = f"{nr_free_pages * page_size / 2**20:.2f}"
    nr_free_pct = f"{nr_free_pages / totalram_pages(cmd.ctx.prog) * 100:.2f}"
    nr_used_pages = totalram_pages(cmd.ctx.prog) - nr_free_pages
    nr_used_mib = f"{nr_used_pages * page_size / 2**20:.2f}"
    nr_used_pct = f"{nr_used_pages / totalram_pages(cmd.ctx.prog) * 100:.2f}"

    # NR_KERNEL_STACK/NR_KERNEL_STACK_KB migrated from zone to node
    # in 991e7673859e, but RHEL 8 kernels are not very consistent with this
    vmstat_text = cmd.ctx.prog.symbol("vmstat_text")
    for i, ptr in enumerate(
        range(vmstat_text.address, vmstat_text.address + vmstat_text.size, cmd.ctx.get_word_size())
    ):
        string = Object(cmd.ctx.prog, "char **", ptr)[0].string_().decode()
        if string == "nr_kernel_stack":
            if i in (9, 16):
                if nr_kernel_stack_in_kb:
                    kernel_stack_kb = zone_stats[nr_kernel_stack_idx].counter.value_()
                    kernel_stack_pages = int(kernel_stack_kb * 2**10 / page_size)
                else:
                    kernel_stack_pages = zone_stats[nr_kernel_stack_idx].counter.value_()
                    kernel_stack_kb = kernel_stack_pages * page_size / 2**10
            elif i in (53, 54, 55):
                kernel_stack_kb = node_stats[nr_kernel_stack_idx].counter.value_()
                kernel_stack_pages = int(kernel_stack_kb * 2**10 / page_size)
            else:
                raise NotImplementedError(f"`nr_kernel_stack` text index is not known: {i}")

    nr_kernel_stack_pages = f"{kernel_stack_pages}"
    nr_kernel_stack_mib = f"{kernel_stack_kb / 2**10:.2f}"
    nr_kernel_stack_pct = f"{kernel_stack_pages / totalram_pages(cmd.ctx.prog) * 100:.2f}"

    table.add_row(["TOTAL", totalram_pages(cmd.ctx.prog), totalram_mib, ""])
    table.add_row(["USED", nr_used_pages, nr_used_mib, nr_used_pct])
    table.add_row(["FREE", nr_free_pages, nr_free_mib, nr_free_pct])
    table.add_row(["", "", "", ""])
    table.add_row(["KSTACK", nr_kernel_stack_pages, nr_kernel_stack_mib, nr_kernel_stack_pct])

    # TODO: add rest from /proc/meminfo

    cmd.println(table.get_string())

    return 0
