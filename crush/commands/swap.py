#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from typing import Optional

from drgn.helpers.linux.fs import d_path
from prettytable import PrettyTable

from crush.commands.common import Command


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="swap", description="Show swap information")
    try:
        _ = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    nr_swapfiles = cmd.ctx.prog["nr_swapfiles"]
    swap_info = cmd.ctx.prog["swap_info"]
    page_size = cmd.ctx.get_page_size()

    swaps = []

    for i in range(0, nr_swapfiles):
        swap_info_struct = swap_info[i]
        swap_info_struct_addr = hex(swap_info_struct.value_())
        flags = hex(swap_info_struct.flags)
        prio = swap_info_struct.prio.value_()
        total = swap_info_struct.pages.value_() * page_size
        total_mib = f"{total / 2**20:.2f}"
        inuse = swap_info_struct.inuse_pages.value_() * page_size
        inuse_mib = f"{inuse / 2**20:.2f}"
        pct_used = f"{inuse / total * 100:.2f}"
        file = d_path(swap_info_struct.swap_file.f_path).decode()
        swaps.append([swap_info_struct_addr, flags, prio, total_mib, inuse_mib, pct_used, file])

    if swaps:
        table = PrettyTable()
        table.border = False
        table.field_names = [
            "swap_info_struct",
            "FLAGS",
            "PRIO",
            "TOTAL, MiB",
            "IN USE, MiB",
            "% USED",
            "FILE",
        ]
        table.align = "r"

        for j in swaps:
            table.add_row(j)

        cmd.println(table.get_string())

        return 0

    cmd.println("(no swap)")

    return 0
