#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from typing import Optional

from crush.commands.common import Command
from crush.helpers.sched import task_handle_to_task, task_header


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="set", description="Set internal variables")
    parser.add_argument("pid", nargs="?", default=None, help="set context to a given task")
    parser.add_argument("--panic", "-p", action="store_true", help="set context to a panic task")
    try:
        args = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    if args.pid is not None:
        cmd.ctx.current = task_handle_to_task(cmd.ctx, args.pid)
    elif args.panic:
        cmd.ctx.current = cmd.ctx.prog.crashed_thread().object

    cmd.println(task_header(cmd.ctx.current))

    return 0
