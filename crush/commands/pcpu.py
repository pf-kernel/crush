#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from typing import Optional

from drgn import Object
from drgn.helpers.linux.cpumask import for_each_online_cpu
from drgn.helpers.linux.percpu import percpu_counter_sum
from prettytable import PrettyTable

from crush.commands.common import Command
from crush.helpers.common import parse_cpu_range, resolve


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(
        prog="pcpu",
        description="""Calculate sum of a per-CPU counter.
        Show per-CPU offsets if no address argument is given""",
    )
    parser.add_argument(
        "--cpu",
        "-C",
        dest="cpu_filters",
        action="extend",
        nargs="+",
        help="show per-CPU offsets for given CPUs only",
    )
    parser.add_argument("address", nargs="?", default=None, help="struct percpu_counter address")
    try:
        args = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    if args.address is None:
        cpu_list = []
        if args.cpu_filters:
            for str_arg in args.cpu_filters:
                cpu_list.extend(parse_cpu_range(str_arg))

        table = PrettyTable()
        table.align = "l"
        table.border = False
        table.field_names = ["CPU", "OFFSET"]
        table.align["CPU"] = "r"  # type: ignore

        for cpu in for_each_online_cpu(cmd.ctx.prog):
            if cpu_list and cpu not in cpu_list:
                continue
            table.add_row([cpu, hex(cmd.ctx.prog["__per_cpu_offset"][cpu].value_())])

        cmd.println(table.get_string())

        return 0

    addrs = []
    if args.address == "-" and cmd.stdin is not None:
        for i in cmd.stdin:
            if i != "":
                addrs += resolve(cmd.ctx, i.strip())
    else:
        addrs = resolve(cmd.ctx, args.address)

    table = PrettyTable()
    table.align = "r"
    table.border = False
    table.header = False

    for addr in addrs:
        vaddr = addr.get_addr()
        fbc = Object(cmd.ctx.prog, "struct percpu_counter *", vaddr)
        pcpu_sum = percpu_counter_sum(fbc)
        table.add_row([hex(vaddr) + ":", pcpu_sum])

    cmd.println(table.get_string())

    return 0
