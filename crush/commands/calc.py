#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from typing import List, Optional

from asteval import Interpreter
from prettytable import PrettyTable

from crush.commands.common import Command


def bits_set(num: int) -> List[int]:
    ret: List[int] = []
    if num < 0:
        num = abs(num)
    while num:
        bit = num & (~num + 1)
        ret.insert(0, bit.bit_length() - 1)
        num ^= bit
    return ret


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="calc", description="Evaluate math expression")
    parser.add_argument("-b", "--binary", action="store_true", help="show result in binary")
    parser.add_argument("-d", "--decimal", action="store_true", help="show result in decimal")
    parser.add_argument("-o", "--octal", action="store_true", help="show result in octal")
    parser.add_argument("-x", "--hex", action="store_true", help="show result in hex")
    parser.add_argument("expression", nargs="+", help="math expression to evaluate")
    try:
        args = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    aeval = Interpreter()
    val = aeval(" ".join(args.expression))

    show_default = not (args.hex or args.decimal or args.octal or args.binary)

    table = PrettyTable()
    table.border = False
    table.header = False
    table.align = "r"

    if show_default:
        table.add_row(["Hex:", hex(int(val))])
    else:
        if args.hex:
            table.add_row(["Hex:", hex(int(val))])
        if args.decimal:
            table.add_row(["Dec:", val])
        if args.octal:
            table.add_row(["Oct:", oct(int(val))])
        if args.binary:
            table.add_row(["Bin:", bin(int(val))])
            table.add_row(["Bits set:", bits_set(int(val))])

    table.max_width = 70
    return cmd.println(table.get_string())
