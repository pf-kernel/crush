#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from datetime import datetime, timezone
from typing import Optional

from drgn import Program
from drgn.helpers.linux.printk import get_dmesg, get_printk_records

from crush.commands.common import Command
from crush.helpers.time import get_timekeeper, get_uptime_ns


def get_dmesg_human_ts(prog: Program, boot_time_s: int) -> str:
    lines = []
    for record in get_printk_records(prog):
        timestamp = (
            datetime.fromtimestamp(boot_time_s + (record.timestamp // 1_000_000_000), timezone.utc)
            .astimezone()
            .strftime("%a %b %e %T %Z %Y")
        )
        lines.append(f'[{timestamp}] {record.text.decode(errors="replace")}')
    lines.append("")  # So we get a trailing newline.
    return "\n".join(lines)


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="dmesg", description="Show kernel long content")
    parser.add_argument(
        "--human-timestamps", "-T", action="store_true", help="print human-readable timestamps"
    )
    try:
        args = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    if not args.human_timestamps:
        # Print default timestamps
        cmd.println(get_dmesg(cmd.ctx.prog).decode(errors="replace"))
    else:
        # Print human-readable timestmaps
        timekeeper = get_timekeeper(cmd.ctx)
        uptime_ns = get_uptime_ns(cmd.ctx)
        uptime_s = uptime_ns // 1_000_000_000
        boot_time_s = timekeeper.xtime_sec.value_() - uptime_s

        cmd.println(get_dmesg_human_ts(cmd.ctx.prog, boot_time_s))

    return 0
