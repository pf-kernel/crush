#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from typing import Optional

from drgn import Architecture, FaultError, stack_trace
from prettytable import PrettyTable

from crush.commands.common import Command
from crush.commands.rd import read_content
from crush.helpers.common import Address, address_to_module_name, demangle, wreg
from crush.helpers.sched import task_header, tasks_from_stdin

# TODO: trampolines? something else?
PPC64_SP_OFFSET = 0x18


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="bt", description="Show task stack trace")
    parser.add_argument(
        "pid", nargs="*", default=None, help="PID or struct task_struct address(es)"
    )
    parser.add_argument("-i", "--inline", action="store_true", help="show inline frames")
    parser.add_argument(
        "-r",
        "--registers",
        action="store_true",
        help="show registers within each frame",
    )
    parser.add_argument(
        "-f", "--frame-content", action="store_true", help="show each frame content"
    )
    parser.add_argument(
        "-s",
        "--stringify",
        action="store_true",
        help="try to convert frame content binary data into ASCII",
    )
    parser.add_argument(
        "-S",
        "--symbolify",
        action="store_true",
        help="resolve symbols within frame content",
    )
    try:
        args = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    tasks = tasks_from_stdin(cmd.ctx, args.pid, cmd.stdin)

    for task in tasks:
        cmd.println(task_header(task))

        trace = []
        try:
            trace = stack_trace(task)
        except FaultError:
            cmd.println("(no stack)")
            continue
        # TODO: narrow down the exception
        except Exception as exc:  # pylint: disable=broad-except
            cmd.errln(f"Unable to get a stack trace: {exc}")
            continue

        for i, frame in enumerate(trace):
            if not args.inline and frame.is_inline:
                continue

            # TODO: stop-gap against infinite s390x stack unwinding
            if frame.pc == 0:
                break

            match cmd.ctx.prog.platform.arch:
                case Architecture.X86_64:
                    stack_ptr = frame.registers()["rsp"]
                case Architecture.PPC64:
                    stack_ptr = frame.registers()["r1"] + PPC64_SP_OFFSET
                case Architecture.S390X:
                    stack_ptr = frame.registers()["r15"]
                case Architecture.AARCH64:
                    stack_ptr = frame.registers()["sp"]
                case _:
                    stack_ptr = None

            # TODO: stop-gap against infinite s390x stack unwinding
            if stack_ptr == 0:
                break

            frame_num = f"#{i}"
            frame_line = f"{frame_num:>4s}"

            if stack_ptr is not None:
                frame_line += " " + f"[{wreg(cmd.ctx, stack_ptr)}]"

            demangled = demangle(cmd.ctx, frame.pc)
            if demangled is not None:
                frame_line += " " + f"at {demangled}"

            try:
                src_file, src_line, src_column = frame.source()
            except LookupError:
                src_file = None
                src_line = 0
                src_column = 0

            if src_file is not None:
                frame_line += " " + "in" + " " + src_file
                if src_line != 0:
                    frame_line += f":{src_line}"
                    if src_column != 0:
                        frame_line += f":{src_column}"
            else:
                frame_line += " " + wreg(cmd.ctx, frame.pc)

            if frame.is_inline:
                frame_line += " " + "(inline)"

            match cmd.ctx.prog.platform.arch:
                case Architecture.X86_64 | Architecture.S390X | Architecture.AARCH64:
                    module_name = address_to_module_name(cmd.ctx, frame.pc)
                    if module_name is not None:
                        frame_line += " " + f"[{module_name}]"
                case Architecture.PPC64:
                    # TODO: avoid FaultError "No way to translate" for now
                    pass

            cmd.println(frame_line)

            if (not frame.is_inline and frame.interrupted) or args.registers:
                table = PrettyTable()
                table.border = False
                table.header = False

                regs = frame.registers()

                row = []
                j = 0
                for reg in regs:
                    row.append(reg + ":")
                    row.append(wreg(cmd.ctx, regs[reg]))
                    j += 1
                    if j > 2:
                        table.add_row(row)
                        row = []
                        j = 0

                if j != 0:
                    for _ in range(0, 3 - j):
                        row.append("")
                        row.append("")
                if row:
                    table.add_row(row)

                table.align = "l"
                table.align["Field 1"] = "r"  # type: ignore
                table.align["Field 3"] = "r"  # type: ignore
                table.align["Field 5"] = "r"  # type: ignore
                cmd.println(table.get_string())

            if args.frame_content and not frame.is_inline and i < len(trace) - 1:
                next_frame = trace[i + 1]

                match cmd.ctx.prog.platform.arch:
                    case Architecture.X86_64:
                        next_stack_ptr = next_frame.registers()["rsp"]
                        frame_size = ((next_stack_ptr - stack_ptr) // 8) % 2**64
                    case Architecture.PPC64:
                        next_stack_ptr = next_frame.registers()["r1"] + PPC64_SP_OFFSET
                        frame_size = ((next_stack_ptr - stack_ptr) // 8) % 2**64
                    case Architecture.S390X:
                        next_stack_ptr = next_frame.registers()["r15"]
                        frame_size = ((next_stack_ptr - stack_ptr) // 8) % 2**64
                    case Architecture.AARCH64:
                        next_stack_ptr = next_frame.registers()["sp"]
                        frame_size = ((next_stack_ptr - stack_ptr) // 8) % 2**64
                    case _:
                        next_stack_ptr = None
                        frame_size = None

                # 16 KiB
                if frame_size is not None and frame_size < 0x4000:
                    buf = read_content(
                        cmd.ctx,
                        [Address(None, None, stack_ptr)],
                        frame_size,
                        None,
                        False,
                        args.stringify,
                        args.symbolify,
                    )
                    table = PrettyTable()
                    table.border = False
                    table.header = False
                    table.align = "l"
                    for row in buf:
                        table.add_row(row)
                    cmd.println(table.get_string())

        cmd.println("")

    return 0
