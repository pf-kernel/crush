#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from datetime import datetime, timezone
from typing import Optional

from drgn.helpers.common.format import number_in_binary_units
from drgn.helpers.linux.cpumask import for_each_online_cpu, for_each_possible_cpu
from drgn.helpers.linux.mm import totalram_pages
from drgn.helpers.linux.pid import for_each_task
from drgn.helpers.linux.sched import loadavg
from prettytable import PrettyTable

from crush.commands.common import Command
from crush.helpers.time import get_timekeeper, get_uptime_ns


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="info", description="Show generic information about vmcore")
    try:
        _ = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    table = PrettyTable()
    table.border = False
    table.header = False

    timekeeper = get_timekeeper(cmd.ctx)

    # TODO: panic string / task (?), tainted mark

    table.add_row(["VMCORE:", cmd.ctx.get_vmcore()])
    table.add_row(["VMLINUX:", cmd.ctx.get_vmlinux()])

    timestamp_str = (
        datetime.fromtimestamp(timekeeper.xtime_sec.value_(), timezone.utc)
        .astimezone()
        .strftime("%a %b %e %T %Z %Y")
    )
    table.add_row(["DATE:", timestamp_str])

    uptime_ns = get_uptime_ns(cmd.ctx)
    uptime_s = uptime_ns // 1_000_000_000
    days = uptime_s // 86400
    uptime_s -= days * 86400
    hours = uptime_s // 3600
    uptime_s -= hours * 3600
    mins = uptime_s // 60
    secs = uptime_s - mins * 60
    uptime_str = ""
    if days != 0:
        uptime_str += f'{days} day{"" if days == 1 else "s"}' + ", "
    uptime_str += f"{hours:02}:{mins:02}:{secs:02}"
    table.add_row(["UPTIME:", uptime_str])

    table.add_row(["HOSTNAME:", cmd.ctx.prog["init_uts_ns"].name.nodename.string_().decode()])
    table.add_row(["RELEASE:", cmd.ctx.prog["init_uts_ns"].name.release.string_().decode()])
    table.add_row(["VERSION:", cmd.ctx.prog["init_uts_ns"].name.version.string_().decode()])

    online_cpus = len(list(for_each_online_cpu(cmd.ctx.prog)))
    possible_cpus = len(list(for_each_possible_cpu(cmd.ctx.prog)))
    cpus_line = f"{online_cpus}"
    if online_cpus != possible_cpus:
        cpus_line += " " + f"online, {possible_cpus - online_cpus} offline"

    table.add_row(["CPUS:", cpus_line])

    table.add_row(
        [
            "MEMORY:",
            f"{number_in_binary_units(totalram_pages(cmd.ctx.prog) * cmd.ctx.get_page_size())}",
        ]
    )
    # pylint: disable=no-value-for-parameter
    table.add_row(["TASKS:", f"{len(list(for_each_task(cmd.ctx.prog)))}"])
    table.add_row(["LOADAVG:", ", ".join(f"{i:.2f}" for i in loadavg(cmd.ctx.prog))])
    table.add_row(["CONFIG_HZ:", cmd.ctx.get_config_hz()])

    table.align["Field 1"] = "r"
    table.align["Field 2"] = "l"

    cmd.println(table.get_string())

    return 0
