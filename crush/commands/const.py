#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from typing import Optional

from crush.commands.common import Command
from crush.context import Context


def get_const(ctx: Context, string: str, is_hex: bool) -> str:
    val = ctx.prog.constant(string).value_()

    if is_hex:
        return hex(val)

    return str(val)


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="const", description="Show given constant")
    parser.add_argument("-x", "--hex", action="store_true", help="show value in hex")
    parser.add_argument("const", nargs="+", help="constant name")
    try:
        args = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    for i in args.const:
        if i == "-" and cmd.stdin is not None:
            for j in cmd.stdin:
                if j != "":
                    cmd.println(get_const(cmd.ctx, j.strip(), args.hex))
        else:
            cmd.println(get_const(cmd.ctx, i, args.hex))

    return 0
