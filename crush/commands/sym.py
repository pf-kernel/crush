#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from typing import Optional

from crush.commands.common import Command
from crush.helpers.common import demangle, resolve, try_int


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="sym", description="Show information on a given symbol")
    parser.add_argument("sym", nargs="+", help="symbol name or address")
    try:
        args = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    syms = []
    for i in args.sym:
        if i == "-" and cmd.stdin is not None:
            for j in cmd.stdin:
                if j != "":
                    syms.append(j)
        else:
            syms.append(i)

    for i in syms:
        val = try_int(i)
        if val is None:
            addrs = resolve(cmd.ctx, i)
            for j in addrs:
                addr = j.get_addr()
                sym_str = hex(addr)
                try:
                    sym_str += "," + " " + repr(cmd.ctx.prog.symbol(i))
                except LookupError:
                    # TODO: a more elegant way to handle per-CPU variables like `cpu_info`?
                    pass
                cmd.println(sym_str)
        else:
            sym = demangle(cmd.ctx, val)
            if sym is not None:
                cmd.println(sym + "," + " " + repr(cmd.ctx.prog.symbol(val)))

    return 0
