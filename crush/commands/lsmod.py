#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from typing import Optional

from drgn.helpers.common.format import decode_flags
from drgn.helpers.linux.cpumask import for_each_possible_cpu
from drgn.helpers.linux.list import list_for_each_entry
from drgn.helpers.linux.module import for_each_module
from drgn.helpers.linux.percpu import per_cpu_ptr
from prettytable import PrettyTable

from crush.commands.common import Command
from crush.helpers.common import contains
from crush.helpers.module import MOD_INIT_TEXT, MOD_TEXT


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="lsmod", description="List loaded modules")
    parser.add_argument("--tainted", "-T", action="store_true", help="show tainting modules only")
    try:
        args = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    modules = []

    for mod in for_each_module(cmd.ctx.prog):
        if args.tainted and mod.taints == 0:
            continue
        addr = hex(mod.value_())
        name = mod.name.string_().decode()

        if contains(mod, "init_layout") and contains(mod, "core_layout"):
            size = (mod.init_layout.size + mod.core_layout.size).value_()
        elif contains(mod, "init_size") and contains(mod, "core_size"):
            size = (mod.init_size + mod.core_size).value_()
        elif contains(mod, "mem"):
            size = (mod.mem[MOD_INIT_TEXT].size + mod.mem[MOD_TEXT].size).value_()
        else:
            raise NotImplementedError("`struct module` layout is not supported")

        if contains(mod, "refcnt"):
            refcnt = mod.refcnt.counter.value_() - 1
        elif contains(mod, "refptr"):
            refcnt = 0
            for i in for_each_possible_cpu(cmd.ctx.prog):
                pmod = per_cpu_ptr(mod.refptr, i)
                refcnt += pmod.incs.value_()
                refcnt -= pmod.decs.value_()
        else:
            raise NotImplementedError("`struct module` layout is not supported")

        taints = decode_flags(mod.taints, [("P", 0), ("O", 12), ("U", 13), ("T", 29)])
        used_by = []
        for use in list_for_each_entry(
            "struct module_use", mod.source_list.address_of_(), "source_list"
        ):
            used_by.append(use.source.name.string_().decode())
        modules.append([addr, name, size, refcnt, taints, ", ".join(used_by)])

    if modules:
        table = PrettyTable()
        table.border = False
        table.field_names = ["ADDR", "NAME", "SIZE", "REFCNT", "TAINTS", "USED BY"]
        table.align = "l"

        for j in modules:
            table.add_row(j)

        cmd.println(table.get_string())

        return 0

    if args.tainted:
        cmd.println("(no tainted modules)")

        return 0

    cmd.println("(no modules)")

    return 0
