#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from typing import Optional

from drgn import Object

# from drgn.helpers.linux.radixtree import radix_tree_for_each
from drgn.helpers.linux.rbtree import rbtree_inorder_for_each
from prettytable import PrettyTable

from crush.commands.common import Command
from crush.helpers.common import resolve


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="tree", description="Iterate over a tree")
    parser.add_argument("-t", "--type", choices=["ra", "rb"], help="tree type")
    # TODO: add offset, reverse options, xarray, rb_node as starting point
    parser.add_argument("root", help="root address")
    try:
        args = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    table = PrettyTable()
    table.border = False
    table.header = False
    table.align = "l"

    addrs = resolve(cmd.ctx, args.root)
    for i in addrs:
        addr = i.get_addr()

        if args.type == "ra":
            # TODO: needs verification
            pass
        #            radix_tree_root = Object(cmd.ctx.prog, "struct radix_tree_root *", addr)
        #            for i in radix_tree_for_each(radix_tree_root):
        #                print(i)
        elif args.type == "rb":
            rb_root = Object(cmd.ctx.prog, "struct rb_root *", addr)
            for j in rbtree_inorder_for_each(rb_root):
                table.add_row([hex(j.value_())])

    cmd.println(table.get_string())

    return 0
