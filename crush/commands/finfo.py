#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from typing import Optional

from drgn import Object
from drgn.helpers.linux.fs import d_path
from prettytable import PrettyTable

from crush.commands.common import Command
from crush.helpers.common import try_int


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="finfo", description="Show information about file(s)")
    # TODO: -i for struct inode, -f for struct file
    parser.add_argument(
        "--dentry",
        "-d",
        action="store_true",
        help="Treat input as struct dentry address(es)",
    )
    parser.add_argument("addrs", nargs="*", default=None, help="Memory address(es)")
    try:
        args = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    if args.addrs is None and cmd.stdin is None:
        cmd.errln("No input given")
        return -1

    addrs = []

    if args.addrs is not None:
        for i in args.addrs:
            if i == "-":
                if cmd.stdin is not None:
                    for j in cmd.stdin:
                        if not j == "":
                            addrs.append(try_int(j))
            else:
                addrs.append(try_int(i))
    else:
        if cmd.stdin is not None:
            for i in cmd.stdin:
                if not i == "":
                    addrs.append(try_int(i))

    table = PrettyTable()
    table.border = False
    table.align = "l"

    if args.dentry:
        table.field_names = ["DENTRY", "INODE", "SUPERBLOCK", "PATH"]

        for addr in addrs:
            dentry = Object(cmd.ctx.prog, "struct dentry *", addr)
            inode = dentry.d_inode
            superblock = dentry.d_sb
            path = d_path(dentry).decode()
            # TODO: same trick as in `files` command; unify?
            if path in ("[anon_inodefs]", "[sockfs]"):
                path = dentry.d_name.name.string_().decode()

            table.add_row(
                [
                    hex(dentry.value_()),
                    hex(inode.value_()),
                    hex(superblock.value_()),
                    path,
                ]
            )

    cmd.println(table.get_string())
    cmd.println("")

    return 0
