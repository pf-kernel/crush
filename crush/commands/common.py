#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

import readline
from importlib import import_module
from pathlib import Path
from shlex import shlex
from subprocess import run
from sys import stderr
from traceback import print_exc
from typing import Dict, List, Optional, Tuple

from drgn import Object
from xdg import BaseDirectory

from crush.context import Context
from crush.textbuffer import TextBuffer

Commands: Dict[str, List] = {
    "ascii": [],
    "bt": [],
    "calc": [],
    "cmdline": [],
    "const": [],
    "dis": [],
    "dmesg": [
        "log",
    ],
    "ext": [],
    "files": [],
    "finfo": [],
    "fn": [],
    "help": [],
    "info": [],
    "lag": [],
    "list": [],
    "lsmod": [
        "mod",
    ],
    "mem": [],
    "mount": [],
    "pcpu": [],
    "ps": [],
    "quit": [
        "exit",
        "q",
    ],
    "rd": [],
    "search": [],
    "set": [],
    "slabs": [],
    "struct": [],
    "sym": [],
    "swap": [],
    "taint": [],
    "tree": [],
    "type": [],
    "uname": [],
    "var": [],
    "vtop": [],
}


def print_err(*args, **kwargs):
    print(*args, file=stderr, **kwargs)


def prettify(string: str) -> List[str]:
    ret = []

    for line in string.splitlines():
        ret.append(line.rstrip())

    return ret


class Command:
    def __init__(self, ctx: Context, argv: List[str], stdin: Optional[TextBuffer], piped: bool):
        self.ctx = ctx
        self.argv = argv
        self.stdin = stdin
        self.piped = piped
        self.stdout = TextBuffer()

    def println(self, string: str):
        for line in prettify(string):
            if self.piped:
                self.stdout.append(line)
            else:
                print(line)

    def errln(self, string: str):
        for line in prettify(string):
            print_err(line)


def wrapper(cmd: Command) -> Tuple[Optional[TextBuffer], Optional[int]]:
    try:
        module = import_module(f"crush.commands.{cmd.argv[0]}")
    except ModuleNotFoundError as exc:
        print_err(exc)
        if cmd.ctx.debug_mode:
            print_exc()
        return None, -1

    try:
        func = getattr(module, "main")
    except AttributeError as exc:
        print_err(exc)
        if cmd.ctx.debug_mode:
            print_exc()
        return None, -2

    try:
        ret = func(cmd)
    except Exception as exc:  # pylint: disable=broad-except
        print_err(f"Exception occurred while executing command: {type(exc).__name__}: {exc}")
        if cmd.ctx.debug_mode:
            print_exc()
        ret = -255

    return cmd.stdout, ret


def struct_exists(ctx: Context, name: str) -> bool:
    try:
        name = name.split(".")[0]
        _ = Object(ctx.prog, f"struct {name} *", 0)
    except (LookupError, SyntaxError):
        return False

    return True


def var_exists(ctx: Context, name: str) -> bool:
    # probably, one really wanted `head` command from outside
    # but not struct hash *head[32] from init/initramfs.c
    var_overrides = ["head"]

    if name in var_overrides:
        return False

    try:
        _ = ctx.prog.variable(name)
    except (LookupError, SyntaxError):
        return False

    return True


def type_exists(ctx: Context, name: str) -> bool:
    try:
        name = name.split(".")[0]
        _ = ctx.prog.type(name)
    except (LookupError, SyntaxError):
        return False

    return True


def const_exists(ctx: Context, name: str) -> bool:
    try:
        _ = ctx.prog.constant(name)
    except (LookupError, SyntaxError):
        return False

    return True


def fn_exists(ctx: Context, name: str) -> bool:
    try:
        _ = ctx.prog.function(name)
    except (LookupError, SyntaxError):
        return False

    return True


def resolve(ctx: Context, args: List[str]) -> Tuple[str, bool, bool, Optional[List[str]]]:
    cmd = args[0]

    for resolution, aliases in Commands.items():
        if cmd == resolution:
            return resolution, False, False, None
        if cmd in aliases:
            return resolution, True, False, None

    if not cmd.startswith("!"):
        if len(args) == 2 and struct_exists(ctx, cmd):
            return "struct", False, True, None

        if var_exists(ctx, cmd):
            return "var", False, True, None

        if type_exists(ctx, f"struct {cmd}"):
            return "type", False, True, ["struct"]

        if type_exists(ctx, cmd):
            return "type", False, True, None

        if const_exists(ctx, cmd):
            return "const", False, True, None

        if fn_exists(ctx, cmd):
            return "fn", False, True, None

        return "execve", False, True, None

    return "execve", False, False, None


def command(ctx: Context, val: str) -> bool:
    stdin: Optional[TextBuffer] = TextBuffer()
    stdout = None

    if val.strip() == "":
        return False

    parser = shlex(instream=val, posix=True, punctuation_chars=True)
    parser.wordchars += "!+:,[]"

    # independent sets of commands (separated by `;`)
    clusters = []
    tokens: List[str] = []
    while True:
        token = parser.get_token()
        if token is parser.eof:
            break

        if token == ";":
            clusters.append(tokens)
            tokens = []
        elif token:
            tokens.append(token)
    if tokens:
        clusters.append(tokens)

    for cluster in clusters:
        commands = []
        argv: List[str] = []
        skip_once = False
        for index, arg in enumerate(cluster):
            if skip_once:
                skip_once = False
                continue

            if arg == "|":
                if argv:
                    commands.append(argv)
                argv = []
            elif arg in [">", ">>", "<", "<<"]:
                # TODO: handle stdin/stdout redirection
                #       currently can be worked around by `cat … | …` and `… | tee …`
                try:
                    _direction = cluster[index + 1]  # noqa: F841
                except IndexError:
                    print_err("Syntax error")
                    return False

                commands.append(argv)
                argv = []
                skip_once = True
            else:
                argv.append(arg)
        if argv:
            commands.append(argv)

        for index, argv in enumerate(commands):
            name, aliased, guessed, prefix = resolve(ctx, argv)
            piped = index != len(commands) - 1
            stdout = None
            ret: Optional[int] = None
            match name:
                case "quit":
                    return True
                case "execve":
                    if not guessed:
                        argv[0] = argv[0][1:]

                    if argv[0] in ("less", "more"):
                        # TODO: support pagers, preferably add internal pager
                        print_err("Pager is not supported yet, consider using `tmux` instead")
                        return False

                    inp = None
                    if stdin is not None:
                        inp = "\n".join(stdin).encode()
                    try:
                        # pylint: disable=subprocess-run-check
                        res = run(argv, input=inp, capture_output=True)
                    except Exception as exc:  # pylint: disable=broad-except
                        print_err(exc)
                    else:
                        ret = res.returncode
                        if res.stdout != b"":
                            stdout = TextBuffer()
                            for line in prettify(res.stdout.decode()):
                                stdout.append(line)
                        if not piped:
                            # TODO: not sure if prettifying is justified here
                            if stdout is not None:
                                for chunk in stdout:
                                    for line in prettify(chunk):
                                        print(line)
                        if res.stderr != b"":
                            for line in prettify(res.stderr.decode()):
                                print_err(line)
                case _:
                    if aliased:
                        argv[0] = name
                    if prefix is not None:
                        argv = prefix + argv
                    if guessed:
                        argv = [name] + argv
                    stdout_raw, ret = wrapper(Command(ctx, argv, stdin, piped))
                    stdout = TextBuffer()
                    if stdout_raw is not None:
                        for chunk in stdout_raw:
                            for line in prettify(chunk):
                                stdout.append(line)

            if ret is not None and ret != 0:
                if ret != -255:
                    print_err(f"Command returned a non-zero exit code: {ret}")
                break

            stdin = stdout

    return False


def repl(ctx: Context) -> bool:
    val = input("crush> ")

    return command(ctx, val)


def loop(ctx: Context) -> None:
    histdir = BaseDirectory.xdg_data_home + "/crush"
    histfile = histdir + "/history"
    Path(histdir).mkdir(parents=True, exist_ok=True)
    if Path(histfile).exists():
        readline.read_history_file(histfile)
    readline.set_history_length(1000)

    while True:
        try:
            val = repl(ctx)
            readline.write_history_file(histfile)
            if val:
                return
        except EOFError:
            return
        except KeyboardInterrupt:
            print()
