#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from typing import List, Optional

from capstone import (
    CS_ARCH_ARM64,
    CS_ARCH_PPC,
    CS_ARCH_SYSZ,
    CS_ARCH_X86,
    CS_MODE_32,
    CS_MODE_64,
    CS_MODE_ARM,
    CS_MODE_BIG_ENDIAN,
    CS_MODE_LITTLE_ENDIAN,
    CS_OPT_SYNTAX_ATT,
    Cs,
    CsError,
    CsInsn,
)
from capstone.arm64 import ARM64_OP_IMM
from capstone.ppc import PPC_OP_IMM
from capstone.systemz import SYSZ_OP_IMM
from capstone.x86 import X86_OP_IMM, X86_OP_MEM
from drgn import Architecture, PlatformFlags
from prettytable import PrettyTable

from crush.commands.common import Command
from crush.context import Context
from crush.helpers.common import demangle, try_int


class DisasmLine:
    """
    Represents a disassembler line containing instruction details and demangler hints.

    Attributes
    ----------
    insn: capstone.CsInsn
        CPU instruction
    symbolic: str
        textual representation of CPU instruction address
    hints: List[str]
        textual representation of memory addresses the CPU instruction operates on

    Methods
    -------
    get_hints():
        returns textual representation of memory addresses the CPU instruction operates on
    get_insn_address():
        returns CPU instruction address in memory
    get_symbolic():
        returns textual representation of CPU instruction address
    get_insn_mnemonic():
        returns CPU instruction mnemonic
    get_insn_op_str():
        returns textual representation of CPU instruction operands
    """

    def __init__(self, ctx: Context, arch: int, mode: int, insn: CsInsn):
        self.insn = insn
        demangled = demangle(ctx, insn.address)
        if demangled is None:
            self.symbolic = "?"
        else:
            self.symbolic = demangled
        self.hints = []

        # skip if data instruction is encountered
        try:
            operands = insn.operands
        except CsError:
            return

        # TODO: also other architectures

        for i in operands:
            if arch == CS_ARCH_X86:
                if i.type == X86_OP_IMM:
                    if mode == CS_MODE_64:
                        imm = i.imm % 2**64
                    else:
                        imm = None
                    if imm is not None:
                        demangled = demangle(ctx, imm)
                        if demangled is not None:
                            self.hints.append(demangled)
                elif i.type == X86_OP_MEM:
                    if i.mem.base != 0:
                        reg = insn.reg_name(i.mem.base)
                        if reg == "rip":
                            dst = (insn.address + insn.size + i.mem.disp) % 2**64
                            demangled = demangle(ctx, dst)
                            hint = hex(dst)
                            if demangled is not None:
                                hint += " " + "<" + demangled + ">"
                            self.hints.append(hint)
                    if i.mem.index != 0 and i.mem.disp != 0:
                        dst = i.mem.disp % 2**64
                        demangled = demangle(ctx, dst)
                        hint = hex(dst)
                        if demangled is not None:
                            hint += " " + "<" + demangled + ">"
                        self.hints.append(hint)
            elif arch == CS_ARCH_PPC:
                if i.type == PPC_OP_IMM:
                    imm = i.imm % 2**64
                    demangled = demangle(ctx, imm)
                    if demangled is not None:
                        self.hints.append(demangled)
            elif arch == CS_ARCH_SYSZ:
                if i.type == SYSZ_OP_IMM:
                    imm = i.imm % 2**64
                    demangled = demangle(ctx, imm)
                    if demangled is not None:
                        self.hints.append(demangled)
            elif arch == CS_ARCH_ARM64:
                if i.type == ARM64_OP_IMM:
                    imm = i.imm % 2**64
                    demangled = demangle(ctx, imm)
                    if demangled is not None:
                        self.hints.append(demangled)

        # TODO: resolve to source code file:line:column
        # https://github.com/osandov/drgn/pull/180

    def get_hints(self) -> List[str]:
        return self.hints

    def get_insn_address(self) -> int:
        return self.insn.address

    def get_symbolic(self) -> str:
        return self.symbolic

    def get_insn_mnemonic(self) -> str:
        return self.insn.mnemonic

    def get_insn_op_str(self) -> str:
        return self.insn.op_str


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="dis", description="Disassemble a given function")
    parser.add_argument("fn", metavar="fn[+0xoffset]", help="function name or address")
    try:
        args = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    if "+" in args.fn:
        entity, size_to_read = args.fn.split("+")
        size_to_read = try_int(size_to_read)
        if size_to_read == 0:
            size_to_read = None
    else:
        entity = args.fn
        size_to_read = None

    val = try_int(entity)
    if val is not None:
        symbol_address = val
        if size_to_read is None:
            size_to_read = cmd.ctx.get_word_size()
    else:
        symbol = cmd.ctx.prog.symbol(entity)
        symbol_address = symbol.address
        if size_to_read is None:
            size_to_read = symbol.size

    instr = cmd.ctx.prog.read(symbol_address, size_to_read)

    # declare what RHEL supports
    match cmd.ctx.prog.platform.arch:
        case Architecture.X86_64:
            arch = CS_ARCH_X86
            mode = CS_MODE_64
            syntax = CS_OPT_SYNTAX_ATT
        case Architecture.I386:
            # TODO: not tested
            arch = CS_ARCH_X86
            mode = CS_MODE_32
            syntax = CS_OPT_SYNTAX_ATT
        case Architecture.AARCH64:
            arch = CS_ARCH_ARM64
            mode = CS_MODE_ARM
            syntax = None
        case Architecture.PPC64:
            if cmd.ctx.prog.platform.flags & PlatformFlags.IS_LITTLE_ENDIAN:
                # TODO: issues with slabs and modules;
                #       looks like something is wrong with virtual memory address translation
                #       bt, dis work just fine though
                #       https://github.com/ptesarik/libkdumpfile/issues/58
                arch = CS_ARCH_PPC
                mode = CS_MODE_64 + CS_MODE_LITTLE_ENDIAN
            else:
                # TODO: same ^^
                arch = CS_ARCH_PPC
                mode = CS_MODE_64 + CS_MODE_BIG_ENDIAN
            syntax = None
        case Architecture.S390X:
            arch = CS_ARCH_SYSZ
            mode = CS_MODE_BIG_ENDIAN
            syntax = None
        case _:
            cmd.errln("Unsupported architecture")
            return -1

    dis = Cs(arch, mode)
    if syntax is not None:
        dis.syntax = syntax

    dis.skipdata = True
    dis.skipdata_setup = ("(bad)", None, None)
    dis.detail = True

    disasm_lines = []
    for i in dis.disasm(instr, symbol_address):
        disasm_lines.append(DisasmLine(cmd.ctx, arch, mode, i))

    table = PrettyTable()
    table.border = False
    table.header = False
    table.align = "l"

    for i in disasm_lines:
        if i.get_hints():
            hints = "# " + ",".join(i.get_hints())
        else:
            hints = ""

        table.add_row(
            [
                hex(i.get_insn_address()),
                "<" + i.get_symbolic() + ">:",
                i.get_insn_mnemonic(),
                i.get_insn_op_str(),
                hints,
            ]
        )

    return cmd.println(table.get_string())
