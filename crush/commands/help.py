#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from typing import Optional

from prettytable import PrettyTable

from crush.commands.common import Command, Commands


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="help", description="Show available commands")
    try:
        _ = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    table = PrettyTable()
    table.align = "l"
    table.border = False
    table.header = False

    col = 0
    row = []
    for main_cmd, aliases in Commands.items():
        if not aliases:
            row.append(main_cmd)
        else:
            row.append(main_cmd + "(" + ",".join(aliases) + ")")
        col += 1
        if col >= 10:
            table.add_row(row)
            col = 0
            row = []
    if col != 0:
        for _ in range(col, 10):
            row.append("")
        table.add_row(row)

    msg = f"""Built-ins available:

{table.get_string()}

To get a help on a specific command type `<command> -h`.


Commands (including built-ins, extensions and external commands) can be piped,
for instance:

\t# this will print the backtraces of all running kernel threads
\tcrush> ps -bKR | bt -


To force the command to be interpreted as an external one, prepend it with `!`,
for instance:

\t# this will execute a system-wide `ps` command, not a built-in
\tcrush> !ps


If an ambiguous command is typed in, for instance, `task_struct` instead of
`type struct task_struct`, or `grep` instead of `!grep`, then `crush` guesses
how to interpret the input in the following order:

\t1. built-in
\t2. struct with address (if one argument is provided)
\t3. variable
\t4. struct without address
\t5. type
\t6. constant
\t7. function
\t8. external command"""

    for line in msg.split("\n"):
        cmd.println(line)

    return 0
