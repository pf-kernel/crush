#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from math import ceil
from typing import Optional

from drgn.helpers.linux.pid import for_each_task
from prettytable import PrettyTable

from crush.commands.common import Command
from crush.commands.rd import read_content
from crush.helpers.common import Address
from crush.helpers.sched import task_header


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="search", description="Search for memory content")
    parser.add_argument(
        "-s",
        "--stringify",
        action="store_true",
        help="try to convert binary data into ASCII",
    )
    parser.add_argument(
        "-S",
        "--symbolify",
        action="store_true",
        help="try to resolve addresses into symbols",
    )
    # TODO: search works on task stacks by default
    #       since full memory search is barely usable due to speed
    # TODO: other goodies from crash-util: search throughout all the memory,
    #       physical address, mask etc
    parser.add_argument("value", help="hex sequence to search for")
    try:
        args = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    # TODO: also account for leading zeroes
    what = int(args.value, 16)
    length = ceil(len(hex(what)[2:]) / 2)

    # pylint: disable=no-value-for-parameter
    for task in for_each_task(cmd.ctx.prog):
        stack_address = task.stack
        # TODO: no stack size hardcoding
        stack_size = 0x4000

        stack = cmd.ctx.prog.read(stack_address, stack_size)

        contents = []

        # TODO: little vs big
        # use struct instead of .to_bytes() maybe?
        offset = stack.find(what.to_bytes(length, "little"))
        while not offset == -1:
            content = read_content(
                cmd.ctx,
                [Address(None, None, stack_address + offset)],
                1,
                None,
                False,
                args.stringify,
                args.symbolify,
            )

            # TODO: this won't show what's found in real time
            contents.append(content[0])

            offset = stack.find(what.to_bytes(length, "little"), offset + 1)

        if contents:
            cmd.println(task_header(task))

            table = PrettyTable()
            table.border = False
            table.header = False
            table.align = "r"

            for content in contents:
                table.add_row(content)

            cmd.println(table.get_string())
            cmd.println("")

    return 0
