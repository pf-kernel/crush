#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from binascii import Error, a2b_hex
from sys import byteorder
from typing import Optional

from drgn import PlatformFlags

from crush.commands.common import Command
from crush.helpers.common import bin2ascii


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="ascii", description="Convert raw hex sequence to ASCII")
    parser.add_argument("hex", nargs="+", help="hex sequence to convert")
    try:
        args = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    val = []

    if cmd.ctx.prog.platform.flags & PlatformFlags.IS_LITTLE_ENDIAN:
        vmcore_endianness = "little"
    else:
        vmcore_endianness = "big"

    for i in args.hex:
        try:
            raw = a2b_hex(i)
        except Error as exc:
            cmd.errln(str(exc))
            return -1

        if byteorder == vmcore_endianness:
            raw = raw[::-1]

        asc = bin2ascii(raw)
        val.append(asc)

    cmd.println(" ".join(val))

    return 0
