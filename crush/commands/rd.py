#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from typing import Dict, List, Optional

from prettytable import PrettyTable

from crush.commands.common import Command
from crush.context import Context
from crush.helpers.common import Address, bin2ascii, demangle, resolve, wreg


def read_content(
    ctx: Context,
    addrs: List[Address],
    size: int,
    word_size: Optional[int],
    phys: bool,
    stringify: bool,
    symbolify: bool,
) -> List[List]:
    val = []

    if word_size is None:
        word_size = ctx.get_word_size()

    for i in addrs:
        addr = i.get_addr()

        word: List[Dict] = []
        if stringify:
            word_str = []
        addr_to_show = None
        steps = range(0, int(size))
        for j in steps:
            addr_to_read = addr + j * word_size
            if addr_to_show is None:
                addr_to_show = addr_to_read
            match word_size:
                case 1:
                    word_raw = ctx.prog.read_u8(addr_to_read, physical=phys)
                case 2:
                    word_raw = ctx.prog.read_u16(addr_to_read, physical=phys)
                case 4:
                    word_raw = ctx.prog.read_u32(addr_to_read, physical=phys)
                case 8:
                    word_raw = ctx.prog.read_u64(addr_to_read, physical=phys)
                case _:
                    raise ValueError(f"Unexpected word size: {word_size}")
            if symbolify:
                symbolic = demangle(ctx, word_raw)
                if symbolic is not None:
                    word.append({"resolved": True, "qual": symbolic})
                else:
                    word.append({"resolved": False, "qual": word_raw})
            else:
                word.append({"resolved": False, "qual": word_raw})
            if stringify:
                raw = ctx.prog.read(addr_to_read, word_size, physical=phys)
                word_str.append(bin2ascii(raw))
            if len(word) == 2 or j == steps[-1]:
                if word[0]["resolved"]:
                    row = [hex(addr_to_show) + ":", word[0]["qual"]]
                else:
                    content = word[0]["qual"]
                    row = [
                        hex(addr_to_show) + ":",
                        wreg(ctx, content, word_size, False),
                    ]
                if len(word) == 2:
                    if word[1]["resolved"]:
                        row += [word[1]["qual"]]
                    else:
                        content = word[1]["qual"]
                        row += [wreg(ctx, content, word_size, False)]
                elif j == steps[-1]:
                    row += [""]
                if stringify:
                    text = word_str[0]
                    if len(word) == 2:
                        text += word_str[1]
                    row += [text]
                else:
                    row += [""]

                val.append(row)

                word = []
                if stringify:
                    word_str = []
                addr_to_show = None

    return val


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="rd", description="Read a content of memory")
    parser.add_argument(
        "-s",
        "--stringify",
        action="store_true",
        help="try to convert binary data into ASCII",
    )
    parser.add_argument(
        "-S",
        "--symbolify",
        action="store_true",
        help="try to resolve addresses into symbols",
    )
    parser.add_argument(
        "-p", "--physical", action="store_true", help="treat memory address as physical"
    )
    parser.add_argument(
        "-8",
        "--byte",
        dest="word_size",
        action="store_const",
        const=1,
        help="word size is 1 byte",
    )
    parser.add_argument(
        "-16",
        "--two-bytes",
        dest="word_size",
        action="store_const",
        const=2,
        help="word size is 2 byte",
    )
    parser.add_argument(
        "-32",
        "--four-bytes",
        dest="word_size",
        action="store_const",
        const=4,
        help="word size is 4 byte",
    )
    parser.add_argument(
        "-64",
        "--eight-bytes",
        dest="word_size",
        action="store_const",
        const=8,
        help="word size is 8 byte",
    )
    parser.add_argument("address", help="memory address to read from")
    parser.add_argument("size", nargs="?", default=1, help="amount of words to read")
    try:
        args = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    addrs = []
    if args.address == "-" and cmd.stdin is not None:
        for i in cmd.stdin:
            if i != "":
                addrs += resolve(cmd.ctx, i.strip())
    else:
        addrs = resolve(cmd.ctx, args.address)

    buf = read_content(
        cmd.ctx,
        addrs,
        args.size,
        args.word_size,
        args.physical,
        args.stringify,
        args.symbolify,
    )

    table = PrettyTable()
    table.border = False
    table.header = False
    table.align = "l"

    for i in buf:
        table.add_row(i)

    table.max_width = 40
    cmd.println(table.get_string())

    return 0
