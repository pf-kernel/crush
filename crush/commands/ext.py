#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from importlib.machinery import SourceFileLoader
from importlib.util import module_from_spec, spec_from_loader
from traceback import print_exc
from typing import Optional

from crush.commands.common import Command


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="ext", description="Run external crush Python script")
    parser.add_argument("script", help="path to script")
    parser.add_argument("args", nargs="*", help="script arguments")
    try:
        args = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    loader = SourceFileLoader("extmodule", args.script)
    spec = spec_from_loader("extmodule", loader)
    if spec is not None:
        extmodule = module_from_spec(spec)
        try:
            loader.exec_module(extmodule)
        except FileNotFoundError as exc:
            cmd.println(str(exc))
            return -1

        try:
            return extmodule.main(cmd)
        except Exception as exc:  # pylint: disable=broad-except
            cmd.println(str(exc))
            if cmd.ctx.debug_mode:
                print_exc()
            return -2

    return -3
