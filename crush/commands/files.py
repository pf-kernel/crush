#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from typing import Optional

from drgn.helpers.linux.fs import d_path, for_each_file
from prettytable import PrettyTable

from crush.commands.common import Command
from crush.helpers.common import try_int
from crush.helpers.sched import task_header, tasks_from_stdin


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="files", description="Show task files")
    parser.add_argument(
        "-R",
        "--reference",
        help="Show only files matching the reference "
        + "(struct file, inode, dentry, private_data)",
    )
    parser.add_argument(
        "-S",
        "--silent",
        action="store_true",
        help="Suppress task header and file absence hints if there's nothing to show",
    )
    parser.add_argument(
        "pid", nargs="*", default=None, help="PID or struct task_struct address(es)"
    )
    try:
        args = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    reference = try_int(args.reference)

    tasks = tasks_from_stdin(cmd.ctx, args.pid, cmd.stdin)

    for task in tasks:
        if task.files.value_() == 0:
            files = 0
        else:
            files = len(list(for_each_file(task)))

        if files != 0:
            records = []

            for file_desc, file in for_each_file(task):
                if file.value_() == 0:
                    continue
                if reference is not None:
                    if (
                        file.value_() != reference
                        and file.f_inode.value_() != reference
                        and file.f_path.dentry.value_() != reference
                        and file.private_data.value_() != reference
                    ):
                        continue

                fd_str = hex(file_desc)
                inode = hex(file.f_inode.value_())
                dentry = hex(file.f_path.dentry.value_())
                path = d_path(file.f_path).decode()
                if path in ("[anon_inodefs]", "[sockfs]"):
                    path = file.f_path.dentry.d_name.name.string_().decode()
                file = hex(file.value_())

                records.append([fd_str, file, inode, dentry, path])

            if records:
                cmd.println(task_header(task))

                table = PrettyTable()
                table.border = False
                table.align = "l"
                table.field_names = ["FD", "FILE", "INODE", "DENTRY", "PATH"]

                for record in records:
                    table.add_row(record)

                cmd.println(table.get_string())
                cmd.println("")
            else:
                if not args.silent:
                    cmd.println(task_header(task))
                    cmd.println("(no files to show)")
        else:
            if not args.silent:
                cmd.println(task_header(task))
                cmd.println("(no files)")

    return 0
