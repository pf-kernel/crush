#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from typing import Optional

from crush.commands.common import Command


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="type", description="Show information about a given type")
    parser.add_argument("type", nargs="+", help="type of interest")
    try:
        _ = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    type_def = " ".join(cmd.argv[1:])
    type_name = type_def.split(".", maxsplit=1)[0]
    type_members = ".".join(type_def.split(".")[1:]).split(",")
    for i in type_members:
        type_struct = cmd.ctx.prog.type(type_name)
        offset = 0
        path = []
        if not i == "":
            for j in i.split("."):
                found = False
                for k in type_struct.members:
                    if k.name == j:
                        path.append(k.name)
                        type_struct = k.type
                        offset += k.bit_offset
                        found = True
                        break
                if not found:
                    cmd.errln(f"Member `.{j}` of `{type_name}` doesn't exist")
                    return -1
        if path:
            cmd.println(
                "["
                + hex(int(offset / 8))
                + "]"
                + " "
                + str(type_struct)
                + " "
                + "."
                + ".".join(path)
            )
        else:
            cmd.println(str(type_struct))

    return 0
