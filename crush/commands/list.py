#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from typing import Optional

from drgn import Object
from drgn.helpers import ValidationError
from drgn.helpers.linux.list import validate_list_for_each
from prettytable import PrettyTable

from crush.commands.common import Command
from crush.helpers.common import resolve


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="list", description="Iterate over a linked list")
    # TODO: add offset, reverse options
    parser.add_argument("head", help="struct list_head address")
    try:
        args = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    table = PrettyTable()
    table.border = False
    table.header = False
    table.align = "l"

    addrs = resolve(cmd.ctx, args.head)
    for i in addrs:
        addr = i.get_addr()

        list_head = Object(cmd.ctx.prog, "struct list_head *", addr)
        try:
            for j in validate_list_for_each(list_head):
                table.add_row([hex(j.value_())])
        except ValidationError as exc:
            table.add_row([str(exc)])

    cmd.println(table.get_string())

    return 0
