#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from typing import Optional

from crush.commands.common import Command


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="fn", description="Show given function")
    parser.add_argument("fn", nargs="+", help="function name")
    try:
        args = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    for i in args.fn:
        if i == "-" and cmd.stdin is not None:
            for j in cmd.stdin:
                if j != "":
                    cmd.println(str(cmd.ctx.prog.function(j.strip())))
        else:
            cmd.println(str(cmd.ctx.prog.function(i)))

    return 0
