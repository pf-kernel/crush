#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from typing import Dict, List, Optional

from drgn import Object
from drgn.helpers.linux.mm import totalram_pages
from drgn.helpers.linux.percpu import per_cpu
from prettytable import PrettyTable

from crush.commands.common import Command
from crush.context import Context
from crush.helpers.common import parse_cpu_range
from crush.helpers.sched import (
    is_active,
    is_group_leader,
    is_kthread,
    task_cpu,
    task_niceval,
    task_policy_to_str,
    task_rss,
    task_rtprio,
    task_state_to_char,
    tasks_from_stdin,
)

PS_MEM_NO_MM = -1
PS_MEM_KTHREAD_OR_UTHREAD = -2


def is_eligible(
    ctx: Object,
    task: Object,
    tasks: List[Object],
    cpu_list: List[int],
    active_filters: List[str],
    state_filters: List[str],
    policy_filters: List[str],
    space_filters: List[str],
    other_filters: List[str],
) -> bool:
    cpu = task_cpu(task)
    active = is_active(ctx, task)
    state = task_state_to_char(task)
    policy = task_policy_to_str(task)

    if task not in tasks:
        return False

    if cpu_list and cpu not in cpu_list:
        return False

    if active_filters and not active:
        return False

    if state_filters and state not in state_filters:
        return False

    if policy_filters and policy not in policy_filters:
        return False

    if space_filters and not (
        ("K" in space_filters and is_kthread(task))
        or ("U" in space_filters and not is_kthread(task))
    ):
        return False

    if other_filters and not ("G" in other_filters and is_group_leader(task)):
        return False

    return True


def populate(
    ctx: Context, table: PrettyTable, task: Object, group_leader_eligible: bool, bare: bool
) -> None:
    taskp = hex(task.value_())

    if bare:
        table.add_row([taskp])
    else:
        pid = task.pid.value_()
        ppid = task.real_parent.pid.value_()
        policy = task_policy_to_str(task)
        rt_prio = task_rtprio(task)
        nice_val = task_niceval(task)
        comm = task.comm.string_().decode()
        cpu = task_cpu(task)
        rq_clock = per_cpu(ctx.prog["runqueues"], cpu).clock.value_()
        task_clock = task.sched_info.last_arrival.value_()
        time = rq_clock - task_clock
        state = task_state_to_char(task)
        state += "X" if is_active(ctx, task) else ""
        mm_struct = task.mm
        if not is_kthread(task) and (
            is_group_leader(task) or (not is_group_leader(task) and not group_leader_eligible)
        ):
            rss: Optional[int] = task_rss(task)
            if rss is None:
                rss = PS_MEM_NO_MM
                vsz = PS_MEM_NO_MM
                pmem = float(PS_MEM_NO_MM)
            else:
                page_size = ctx.get_page_size()
                rss *= page_size
                pmem = rss / (totalram_pages(ctx.prog) * ctx.get_page_size()) * 100
                vsz = mm_struct.total_vm.value_() * page_size
        else:
            rss = PS_MEM_KTHREAD_OR_UTHREAD
            vsz = PS_MEM_KTHREAD_OR_UTHREAD
            pmem = float(PS_MEM_KTHREAD_OR_UTHREAD)
        tcomm = comm
        if not is_group_leader(task) and group_leader_eligible:
            tcomm = "+-" + comm
        if rt_prio != 0:
            prio = str(rt_prio)
            nice = "-"
        else:  # rt_prio == 0 means running on CFS, hence nice value applies
            prio = "-"
            nice = str(nice_val)
        table.add_row(
            [time, pid, ppid, cpu, policy, prio, nice, taskp, state, pmem, rss, vsz, tcomm]
        )


def sort_int(val: List[str]) -> int:
    try:
        return int(val[0])
    except ValueError:
        return 0


def format_timestamp(_field: str, value: str) -> str:
    nsecs = int(value)

    msecs = nsecs // 1_000_000

    days = msecs // 86_400_000
    msecs %= 86_400_000

    hours = msecs // 3_600_000
    msecs %= 3_600_000

    mins = msecs // 60_000
    msecs %= 60_000

    secs = msecs // 1_000
    msecs %= 1_000

    return f"[{days: 4d} {hours:02d}:{mins:02d}:{secs:02d}.{msecs:03d}]"


def format_memory(_field: str, value: str) -> str:
    if value == PS_MEM_NO_MM:
        return "-"

    if value == PS_MEM_KTHREAD_OR_UTHREAD:
        return ""

    return str(int(value) // 1_024)


def format_memory_pct(_field: str, value: str) -> str:
    if value == float(PS_MEM_NO_MM):
        return "-"

    if value == float(PS_MEM_KTHREAD_OR_UTHREAD):
        return ""

    return f"{value:.2f}"


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="ps", description="Show information on processes")
    parser.add_argument(
        "pid", nargs="*", default=None, help="PID or struct task_struct address(es)"
    )
    # modifiers
    parser.add_argument("--summary", "-s", action="store_true", help="show task states summary")
    parser.add_argument(
        "--bare",
        "-b",
        action="store_true",
        help="show struct task_struct pointers only",
    )
    parser.add_argument(
        "--by-rss",
        "-r",
        action="store_true",
        help="sort tasks by RSS usage (implies -U and -G)",
    )
    parser.add_argument(
        "--by-vsz",
        "-v",
        action="store_true",
        help="sort tasks by VSZ usage (implies -U and -G)",
    )
    parser.add_argument(
        "--by-time",
        "-t",
        action="store_true",
        help="sort tasks by runqueue timestamp",
    )
    # filters
    # - cpu
    parser.add_argument(
        "--cpu",
        "-C",
        dest="cpu_filters",
        action="extend",
        nargs="+",
        help="show tasks assigned to a CPU",
    )
    # - active
    parser.add_argument(
        "--active",
        "-X",
        dest="active_filters",
        action="append_const",
        const="X",
        help="show active tasks",
    )
    # - state
    parser.add_argument(
        "--running",
        "-R",
        dest="state_filters",
        action="append_const",
        const="R",
        help="show running tasks",
    )
    parser.add_argument(
        "--sleeping",
        "-S",
        dest="state_filters",
        action="append_const",
        const="S",
        help="show tasks in interruptible sleep",
    )
    parser.add_argument(
        "--disksleep",
        "-D",
        dest="state_filters",
        action="append_const",
        const="D",
        help="show tasks in uninterruptible sleep",
    )
    parser.add_argument(
        "--zombie",
        "-Z",
        dest="state_filters",
        action="append_const",
        const="Z",
        help="show zombie tasks",
    )
    parser.add_argument(
        "--parked",
        "-P",
        dest="state_filters",
        action="append_const",
        const="P",
        help="show parked tasks",
    )
    parser.add_argument(
        "--idle",
        "-I",
        dest="state_filters",
        action="append_const",
        const="I",
        help="show idle tasks",
    )
    parser.add_argument(
        "--waking",
        "-W",
        dest="state_filters",
        action="append_const",
        const="W",
        help="show waking tasks",
    )
    parser.add_argument(
        "--wakekill",
        "-A",
        dest="state_filters",
        action="append_const",
        const="A",
        help="show wakekill tasks",
    )
    # - sched policy
    parser.add_argument(
        "--normal",
        "-N",
        dest="policy_filters",
        action="append_const",
        const="OT",
        help="show SCHED_OTHER tasks",
    )
    parser.add_argument(
        "--fifo",
        "-F",
        dest="policy_filters",
        action="append_const",
        const="FF",
        help="show SCHED_FIFO tasks",
    )
    parser.add_argument(
        "--rr",
        "-O",
        dest="policy_filters",
        action="append_const",
        const="RR",
        help="show SCHED_RR tasks",
    )
    parser.add_argument(
        "--batch",
        "-B",
        dest="policy_filters",
        action="append_const",
        const="BA",
        help="show SCHED_BATCH tasks",
    )
    parser.add_argument(
        "--pidle",
        "-L",
        dest="policy_filters",
        action="append_const",
        const="ID",
        help="show SCHED_IDLE tasks",
    )
    parser.add_argument(
        "--deadline",
        "-E",
        dest="policy_filters",
        action="append_const",
        const="DL",
        help="show SCHED_DEADLINE tasks",
    )
    # - kernel/user
    parser.add_argument(
        "--kernel",
        "-K",
        dest="space_filters",
        action="append_const",
        const="K",
        help="show kernel tasks only",
    )
    parser.add_argument(
        "--user",
        "-U",
        dest="space_filters",
        action="append_const",
        const="U",
        help="show userspace tasks only",
    )
    # - other
    parser.add_argument(
        "--group-leader",
        "-G",
        dest="other_filters",
        action="append_const",
        const="G",
        help="hide userspace threads",
    )

    try:
        args = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    sorting = args.by_rss + args.by_vsz + args.by_time
    if sorting > 1:
        cmd.errln("Incompatible arguments")
        return -1

    if args.bare and sorting:
        cmd.errln("Incompatible arguments")
        return -1

    if args.by_rss or args.by_vsz:
        args.group_leader = True
        if not args.other_filters:
            args.other_filters = []
        args.other_filters.append("G")

        args.user = True
        if not args.space_filters:
            args.space_filters = []
        args.space_filters.append("U")

    if (
        args.cpu_filters
        or args.active_filters
        or args.state_filters
        or args.policy_filters
        or args.space_filters
        or args.other_filters
    ) and args.summary:
        cmd.errln("Incompatible arguments")
        return -1

    cpu_list = []
    if args.cpu_filters:
        for str_arg in args.cpu_filters:
            cpu_list.extend(parse_cpu_range(str_arg))

    tasks = tasks_from_stdin(cmd.ctx, args.pid, cmd.stdin, True)

    if args.summary:
        states = {}
        for task in tasks:
            state = task_state_to_char(task)
            if state not in states:
                states[state] = 0
            states[state] += 1

        table = PrettyTable()
        table.header = False
        table.border = False
        for state_char, task_count in states.items():
            table.add_row([state_char + ":", task_count])
        table.align = "l"
        cmd.println(table.get_string())
    else:
        table = PrettyTable()
        table.border = False
        if args.bare:
            table.header = False
        else:
            table.field_names = [
                "TIME",
                "PID",
                "PPID",
                "CPU",
                "SP",
                "PRIO",
                "NICE",
                "TASK",
                "ST",
                "%MEM",
                "RSS",
                "VSZ",
                "COMM",
            ]
        table.align = "l"
        # pylint: disable=unsupported-assignment-operation
        table.align["PRIO"] = "r"  # type: ignore
        table.align["NICE"] = "r"  # type: ignore
        # pylint: enable=unsupported-assignment-operation

        table.custom_format["TIME"] = format_timestamp
        table.custom_format["%MEM"] = format_memory_pct
        table.custom_format["RSS"] = format_memory
        table.custom_format["VSZ"] = format_memory

        tgids: Dict[int, List] = {}
        for task in tasks:
            if task is None:
                continue
            tgidh = task.group_leader.value_()
            if tgidh not in tgids:
                tgids[tgidh] = []
            if task.group_leader == task:
                continue
            tgids[tgidh].append(task)

        for task_address, threads in tgids.items():
            task = Object(cmd.ctx.prog, "struct task_struct *", task_address)
            group_leader_eligible = is_eligible(
                cmd.ctx,
                task,
                tasks,
                cpu_list,
                args.active_filters,
                args.state_filters,
                args.policy_filters,
                args.space_filters,
                args.other_filters,
            )
            if group_leader_eligible:
                populate(cmd.ctx, table, task, True, args.bare)

            for thread in threads:
                if is_eligible(
                    cmd.ctx,
                    thread,
                    tasks,
                    cpu_list,
                    args.active_filters,
                    args.state_filters,
                    args.policy_filters,
                    args.space_filters,
                    args.other_filters,
                ):
                    populate(cmd.ctx, table, thread, group_leader_eligible, args.bare)

        if args.by_rss:
            sortby = "RSS"
        elif args.by_vsz:
            sortby = "VSZ"
        elif args.by_time:
            sortby = "TIME"
        else:
            sortby = None

        cmd.println(table.get_string(sortby=sortby, sort_key=sort_int))

    return 0
