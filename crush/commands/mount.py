#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from typing import Optional

from drgn.helpers.linux.fs import for_each_mount, mount_dst, mount_fstype, mount_src
from prettytable import PrettyTable

from crush.commands.common import Command


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="mount", description="Show mountpoints")
    try:
        _ = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    table = PrettyTable()
    table.border = False
    table.align = "l"

    table.field_names = ["MOUNT", "SUPER_BLOCK", "TYPE", "DEV", "MOUNTPOINT"]

    # TODO: even if prog is given, pylint still complains:
    #       No value for argument 'ns' in function call
    #       ns should have been cmd.ctx.prog["init_task"].nsproxy.mnt_ns,
    #       How does one shut it up except ignoring altogether?
    # pylint: disable=no-value-for-parameter
    for mount_struct in for_each_mount(cmd.ctx.prog):
        mount = hex(mount_struct.value_())
        superblock_ptr = mount_struct.mnt.mnt_sb.value_()
        superblock = hex(superblock_ptr)
        if superblock_ptr != 0:
            mtype = mount_fstype(mount_struct).decode()
            devname = mount_src(mount_struct).decode()
            dirname = mount_dst(mount_struct).decode()
        else:
            mtype = "?"
            devname = "?"
            dirname = "?"
        table.add_row([mount, superblock, mtype, devname, dirname])

    cmd.println(table.get_string())

    return 0
