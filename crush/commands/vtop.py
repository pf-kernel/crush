#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from typing import Optional

from drgn.helpers.linux.mm import (
    follow_page,
    follow_phys,
    page_to_pfn,
    page_to_phys,
    vmalloc_to_page,
)
from prettytable import PrettyTable

from crush.commands.common import Command


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="vtop", description="Translate virtual address to a physical one")
    parser.add_argument(
        "--user", "-u", action="store_true", help="treat address as a userspace one"
    )
    parser.add_argument("address", help="virtual address")
    try:
        args = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    table = PrettyTable()
    table.align = "r"
    table.border = False
    table.header = False

    addr = int(args.address, 16)
    if args.user:
        page = follow_page(cmd.ctx.current.mm, addr)
        phys = follow_phys(cmd.ctx.current.mm, addr)
    else:
        page = vmalloc_to_page(cmd.ctx.prog, addr)
        phys = page_to_phys(page) | (addr & ((1 << cmd.ctx.prog["PAGE_SHIFT"]) - 1))
    pfn = page_to_pfn(page)

    table.add_row(["PAGE", hex(page.value_())])
    table.add_row(["PFN", hex(pfn)])
    table.add_row(["PHYS", hex(phys)])

    cmd.println(table.get_string())

    return 0
