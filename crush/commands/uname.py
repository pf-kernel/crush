#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from typing import Optional

from crush.commands.common import Command


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="uname", description="Show kernel version information")
    try:
        _ = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    cmd.println(cmd.ctx.prog["linux_banner"].string_().decode().replace("\n", ""))

    return 0
