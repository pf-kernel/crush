#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from typing import Optional

from drgn import FaultError, TypeKind
from drgn.helpers.linux.percpu import per_cpu

from crush.commands.common import Command
from crush.helpers.common import parse_cpu_range


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="var", description="Show a content of a given variable")
    parser.add_argument("-x", "--hex", action="store_true", help="show value in hex")
    parser.add_argument("var", help="variable name")
    try:
        args = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    # TODO: can this code be merged with resolve() helper?

    if ":" in args.var:
        varname, cpu = args.var.split(":")
        cpu = parse_cpu_range(cpu)
    else:
        varname = args.var
        cpu = None

    variable = cmd.ctx.prog.variable(varname)
    kind = variable.type_.kind

    match kind:
        case TypeKind.INT:
            if cpu is not None:
                for i in cpu:
                    val = per_cpu(variable, i)
                    if args.hex:
                        cmd.println(hex(val))
                    else:
                        cmd.println(str(val))
            else:
                val = variable.value_()
                if args.hex:
                    cmd.println(hex(val))
                else:
                    cmd.println(str(val))
        case TypeKind.ARRAY:
            elemtype = variable[0].type_.kind
            match elemtype:
                case TypeKind.INT:
                    match variable[0].type_.name:
                        case "char":
                            cmd.println(variable.string_().decode())
                        case _:
                            cmd.println(str(variable))
                case TypeKind.POINTER:
                    try:
                        cmd.println(str(variable))
                    except FaultError:
                        # TODO: array of struct pointers
                        cmd.println(repr(variable))
        case _:
            cmd.println(str(variable))

    return 0
