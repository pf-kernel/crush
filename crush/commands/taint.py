#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from typing import Optional

from drgn.helpers.common.format import decode_flags

from crush.commands.common import Command

taint_flags = [
    ("PROPRIETARY_MODULE", 0),
    ("FORCED_MODULE", 1),
    ("CPU_OUT_OF_SPEC", 2),
    ("FORCED_RMMOD", 3),
    ("MACHINE_CHECK", 4),
    ("BAD_PAGE", 5),
    ("USER", 6),
    ("DIE", 7),
    ("OVERRIDDEN_ACPI_TABLE", 8),
    ("WARN", 9),
    ("CRAP", 10),
    ("FIRMWARE_WORKAROUND", 11),
    ("OOT_MODULE", 12),
    ("UNSIGNED_MODULE", 13),
    ("SOFTLOCKUP", 14),
    ("LIVEPATCH", 15),
    ("AUX", 16),
    ("RANDSTRUCT", 17),
    ("HARDWARE_UNSUPPORTED", 28),
    ("TECH_PREVIEW", 29),
]


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="taint", description="Show and decode tainting flags")
    try:
        _ = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    tainted_mask = cmd.ctx.prog["tainted_mask"].value_()
    if tainted_mask == 0:
        cmd.println("(not tainted)")
        return 0

    cmd.println(hex(tainted_mask) + ": " + decode_flags(tainted_mask, taint_flags))

    return 0
