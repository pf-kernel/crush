#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from re import compile as regex_compile
from typing import Optional, Union

from drgn import Object, cast
from drgn.helpers.linux.list import list_for_each_entry
from drgn.helpers.linux.nodemask import for_each_online_node
from drgn.helpers.linux.slab import for_each_slab_cache
from prettytable import PrettyTable

from crush.commands.common import Command
from crush.helpers.common import contains

OO_SHIFT = 16


def sort_slabs(val: str) -> Union[int, str]:
    try:
        return int(val[0][:-1])
    except ValueError:
        return val


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="slabs", description="Show information about SLAB subsystem")
    parser.add_argument("name", nargs="*", default=None, help="kmem_cache name(s)")
    parser.add_argument(
        "--regex", "-E", action="store_true", help="treat kmem_cache name(s) as regex"
    )
    parser.add_argument(
        "--bare", "-b", action="store_true", help="show `struct kmem_cache` address only"
    )
    parser.add_argument("--by-name", "-n", action="store_true", help="sort by name")
    parser.add_argument("--by-size", "-s", action="store_true", help="sort by memory usage")
    try:
        args = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    page_size = cmd.ctx.get_page_size()

    # upstream kernel commit 4da1984edb
    struct_page = Object(cmd.ctx.prog, "struct page *", 0)
    if contains(struct_page, "next"):
        # RHEL 7
        slab_list_member = "next"
    elif contains(struct_page, "slab_list"):
        # RHEL 8, 9
        slab_list_member = "slab_list"
    elif contains(struct_page, "lru"):
        # v6.2
        slab_list_member = "lru"
    else:
        cmd.errln("Unable to parse struct page")
        return -1

    res = []
    for slab in for_each_slab_cache(cmd.ctx.prog):
        name = slab.name.string_().decode()

        if args.name:
            skip = True
            if args.regex:
                for regex in args.name:
                    if regex_compile(regex).match(name):
                        skip = False
                        break
            else:
                if name in args.name:
                    skip = False

            if skip:
                continue

        addr = hex(slab.value_())
        objsize = slab.object_size.value_()
        slabs = 0
        total = 0
        free = 0
        for node in for_each_online_node(cmd.ctx.prog):
            slabs += slab.node[node].nr_slabs.counter.value_()
            total += slab.node[node].total_objects.counter.value_()
            # TODO: needs verification
            for page in list_for_each_entry(
                "struct page", slab.node[node].partial.address_of_(), slab_list_member
            ):
                if contains(page, "objects"):
                    free += page.objects.value_() - page.inuse.value_()
                else:
                    struct_slab = cast("struct slab *", page)
                    free += struct_slab.objects.value_() - struct_slab.inuse.value_()
        allocated = total - free
        ssize_bytes = 2 ** (slab.oo.x.value_() >> OO_SHIFT) * page_size
        ssize = str(ssize_bytes // 2**10) + "k"
        mem = str(slabs * ssize_bytes // 2**10) + "k"

        res.append([name, addr, objsize, allocated, total, slabs, ssize, mem])

    if args.by_name:
        res.sort(key=lambda x: (x[0]))
    elif args.by_size:
        res.sort(key=lambda x: (int(x[7][:-1])))

    table = PrettyTable()
    table.border = False
    table.align = "r"

    if args.bare:
        table.header = False

        for i in res:
            table.add_row([i[1]])
    else:
        table.field_names = [
            "NAME",
            "KMEM_CACHE",
            "OBJSIZE",
            "ALLOCATED",
            "TOTAL",
            "SLABS",
            "SSIZE",
            "MEM",
        ]

        for i in res:
            table.add_row(i)

    cmd.println(table.get_string())

    return 0
