#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from typing import Optional

from crush.commands.common import Command


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="cmdline", description="Show kernel command line options")
    try:
        _ = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    return cmd.println(cmd.ctx.prog["saved_command_line"].string_().decode())
