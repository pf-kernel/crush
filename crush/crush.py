#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

import tomllib
from argparse import ArgumentParser
from importlib.metadata import PackageNotFoundError, version
from os import walk
from os.path import join
from pathlib import Path
from resource import RLIMIT_NOFILE, getrlimit, setrlimit
from sys import stdout

from drgn import FaultError, MissingDebugInfoError, Program
from drgn.helpers.linux.module import for_each_module
from xdg import BaseDirectory

from crush.commands.common import command, loop, print_err
from crush.context import Context

MOD_SUFFIX = ".ko.debug"


def launch() -> None:
    try:
        # installed package
        app_ver = version(__package__)
    except PackageNotFoundError:
        # development folder
        with open("pyproject.toml", "rb") as prj_file:
            app_ver = tomllib.load(prj_file)["project"]["version"]

    parser = ArgumentParser(description="A drgn-based crash-util replacement")
    parser.add_argument("-v", "--version", action="version", version=f"{__package__} v{app_ver}")
    parser.add_argument("--vmcore", "-c", type=str, required=True, help="path to vmcore")
    parser.add_argument("--vmlinux", "-l", type=str, required=True, help="path to vmlinux")
    parser.add_argument("--mods", "-m", type=str, help="path to modules debuginfo")
    parser.add_argument("--hz", "-z", type=int, help="CONFIG_HZ value")
    parser.add_argument("--crushrc", "-r", type=str, help="path to crushrc")
    parser.add_argument(
        "--debug-mode",
        "-d",
        action="store_true",
        help="enable debug mode for development purposes",
    )
    args = parser.parse_args()

    prog = Program()

    try:
        prog.set_core_dump(args.vmcore)
    except (FileNotFoundError, ValueError) as exc:
        print_err(args.vmcore + ":" + " " + str(exc))
        return

    print("Loading kernel debug info...", end="")
    stdout.flush()

    try:
        prog.load_debug_info([args.vmlinux])
    except MissingDebugInfoError as exc:
        print_err()
        print_err(exc)
        return

    print(" done")

    debug_info = []

    debug_info.append(args.vmlinux)

    loaded_mod_names = []
    try:
        for mod in for_each_module(prog):
            loaded_mod_names.append(mod.name.string_().decode().replace("-", "_"))
    except FaultError:
        # TODO: ppc64le: Cannot get page I/O address: No way to translate
        pass

    mod_debug_info = []
    if args.mods is not None:
        for root, _, files in walk(args.mods):
            for file_name in files:
                if not file_name.endswith(MOD_SUFFIX):
                    continue
                file_name_normalised = file_name.replace("-", "_")
                file_name_normalised = file_name_normalised[
                    : len(file_name_normalised) - len(MOD_SUFFIX)
                ]
                if file_name_normalised in loaded_mod_names:
                    mod_debug_info.append(join(root, file_name))

    nofiles = len(mod_debug_info)
    if nofiles != 0:
        (curr_nofiles_soft, curr_nofiles_hard) = getrlimit(RLIMIT_NOFILE)

        new_nofiles_soft = curr_nofiles_soft
        new_nofiles_hard = curr_nofiles_hard

        if nofiles > curr_nofiles_hard:
            new_nofiles_hard = int(nofiles * 2)
        if nofiles > curr_nofiles_soft:
            new_nofiles_soft = min(int(nofiles * 1.5), curr_nofiles_hard)

        if new_nofiles_soft != curr_nofiles_soft or new_nofiles_hard != curr_nofiles_hard:
            print("Raising RLIMIT_NOFILE...", end="")
            setrlimit(RLIMIT_NOFILE, (new_nofiles_soft, new_nofiles_hard))
            print(" done")

        print(
            f'Loading modules debug info from {nofiles} file{"" if nofiles == 1 else "s"}...',
            end="",
        )
        stdout.flush()

        try:
            prog.load_debug_info(mod_debug_info)
        except MissingDebugInfoError as exc:
            print_err()
            print_err(exc)
            return

        print(" done")

        debug_info += mod_debug_info

    context = Context(prog, args.vmcore, debug_info, args.hz, args.debug_mode)

    print("")
    command(context, "info")
    print("")

    crushrcs = [
        Path.home().joinpath(".crushrc"),
    ]

    if not BaseDirectory.xdg_config_home == "":
        crushrcs.append(BaseDirectory.xdg_config_home + "/crush/crushrc")
    else:
        crushrcs.append(Path.home().joinpath("config/crushrc/crushrc"))

    if args.crushrc is not None:
        crushrcs.append(args.crushrc)

    for crushrc in crushrcs:
        if Path(crushrc).exists():
            with open(crushrc, encoding="utf-8") as file:
                for cmd in file.readlines():
                    if command(context, cmd):
                        return

    loop(context)


if __name__ == "__main__":
    launch()
