#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from argparse import ArgumentParser
from typing import Optional

from crush.commands.common import Command


def main(cmd: Command) -> Optional[int]:
    parser = ArgumentParser(prog="hello", description="Hello World")
    parser.add_argument("args", nargs="*", help="arguments")
    try:
        args = parser.parse_args(cmd.argv[1:])
    except SystemExit:
        return None

    cmd.println("Hello, world! Script arguments are:")
    cmd.println("")
    for argi, arg in enumerate(args.args):
        cmd.println(f"\targv[{argi}] = " + arg)
    cmd.println("")
    cmd.println(f"Lines in script's stdin: {0 if cmd.stdin is None else len(cmd.stdin)}.")
    cmd.println("")

    cmd.println(f'vmcore jiffies: 0x{cmd.ctx.prog["jiffies"].value_():x}')

    return 0
