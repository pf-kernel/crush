#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import Dict, List, Optional

from drgn import PlatformFlags, Program, set_default_prog
from drgn.helpers.linux.kconfig import get_kconfig


class Context:
    """
    Represents a crush context which contains drgn prog, current task, debug information and
    symbols cache.

    Attributes
    ----------
    prog: drgn.Program
        reference to drgn prog
    current: drgn.Object
        reference to a current task
    vmcore: str
        path to a vmcore file
    debug_info: List[str]
        list of files containing debug symbols
    debug_mode: bool
        indicates if extra debugging output is enabled
    symcache: Dict[int, Optional[str]]
        cache of virtual address mappings to their symbolic names + offset
    page_size: int
        size of one page in bytes
    modcache: Dict[int, Optional[str]]
        cache of virtual address mappings to their module names
    word_size: int
        the size of the word on this platform
    config_hz: int
        CONFIG_HZ value

    Methods
    -------
    in_symcache(addr):
        Checks if a symbolic representation of a given address is in cache
    get_from_symcache(addr):
        Gets a symbolic representation of a given address from cache
    put_to_symcache(addr, val):
        Puts a symbolic representation of a given address to cache
    get_page_size():
        Returns a page size
    in_modcache(addr):
        Checks if a module name that corresponds to a given address is in cache
    get_from_modcache(addr):
         Gets a module name that corresponds to a given address from cache
    put_to_modcache(addr, val):
        Puts a module name that corresponds to a given address to cache
    get_word_size():
        Returns the size of the word on this platform
    get_config_hz():
        Returns CONFIG_HZ value
    get_vmcore():
        Returns a path to a vmcore file
    get_vmlinux():
        Returns a path to a vmlinux file
    get_banner():
        Returns kernel banner
    symbol_exists():
        Checks if a given symbol exists
    """

    def __init__(
        self, prog: Program, vmcore: str, debug_info: List[str], config_hz: int, debug_mode: bool
    ):
        self.prog = prog
        set_default_prog(self.prog)
        try:
            self.current = prog.crashed_thread().object
        except Exception:  # pylint: disable=broad-exception-caught
            # ^^ not sure why drgn throws generic exception
            #    https://github.com/osandov/drgn/issues/391
            # TODO: should be safe? Analysing `dmesg` is another option to try
            self.current = prog.thread(1).object
        self.vmcore = vmcore
        self.debug_info = debug_info
        self.debug_mode = debug_mode
        self.symcache: Dict[int, Optional[str]] = {}
        self.page_size = prog["PAGE_SIZE"].value_()
        self.modcache: Dict[int, Optional[str]] = {}
        if not prog.platform.flags & PlatformFlags.IS_64_BIT:
            self.word_size = 4
        else:
            self.word_size = 8

        if config_hz is not None:
            self.config_hz = config_hz
        else:
            self.config_hz = -1
            try:
                self.config_hz = int(get_kconfig(prog)["CONFIG_HZ"])
            except LookupError:
                pass

            if self.config_hz == -1:
                try:
                    # block/mq-deadline.c
                    # static const int write_expire = 5 * HZ
                    self.config_hz = prog["write_expire"].value_() // 5
                except KeyError:
                    pass

            if self.config_hz == -1:
                print("Unable to get CONFIG_HZ value, defaulting to 1000")
                self.config_hz = 1000

    def in_symcache(self, addr: int) -> bool:
        return addr in self.symcache

    def get_from_symcache(self, addr: int) -> Optional[str]:
        return self.symcache[addr]

    def put_to_symcache(self, addr: int, val: Optional[str]) -> None:
        self.symcache[addr] = val

    def get_page_size(self) -> int:
        return self.page_size

    def in_modcache(self, addr: int) -> bool:
        return addr in self.modcache

    def get_from_modcache(self, addr: int) -> Optional[str]:
        return self.modcache[addr]

    def put_to_modcache(self, addr: int, val: Optional[str]) -> None:
        self.modcache[addr] = val

    def get_word_size(self) -> int:
        return self.word_size

    def get_config_hz(self) -> int:
        return self.config_hz

    def get_vmcore(self) -> str:
        return self.vmcore

    def get_vmlinux(self) -> str:
        return self.debug_info[0]

    def symbol_exists(self, sym: str) -> bool:
        try:
            _ = self.prog[sym]
        except KeyError:
            return False
        return True
