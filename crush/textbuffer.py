#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import Any


class TextBuffer:
    """
    Represents a text buffer that is passed between commands as stdin and stdout.

    Attributes
    ----------
    buffer: List[str]
        list of text lines constituting a text buffer
    index: int
        current buffer iterator index

    Methods
    -------
    append(string):
        adds a new text line to the buffer
    """

    def __init__(self, *inp):
        self.buffer = []
        if len(inp) == 1:
            self.__init_arg1(inp)
        self.index = 0

    def __init_arg1(self, args):
        self.append(args[0])

    def append(self, string: Any) -> None:
        self.buffer.append(str(string))

    def __iter__(self):
        return self

    def __next__(self):
        try:
            result = self.buffer[self.index]
        except IndexError as exc:
            self.index = 0
            raise StopIteration from exc

        self.index += 1
        return result

    def __len__(self) -> int:
        return len(self.buffer)
